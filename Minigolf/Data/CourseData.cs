﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Minigolf.Data
{
	public class CourseData
	{
		public string Name { get; set; }

		public HoleData[] Holes { get; set; }
	}
}
