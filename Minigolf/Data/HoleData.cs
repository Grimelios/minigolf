﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Minigolf.Data
{
	public class HoleData
	{
		public int Number { get; set; }
		public int Par { get; set; }

		public string Name { get; set; }
		public string Creator { get; set; }
	}
}
