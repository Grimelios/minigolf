﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Minigolf.Interfaces;
using Minigolf.Structures;

namespace Minigolf.Core
{
	public class Timer : IDynamic
	{
		private Action<float> tick;
		private Action<float> trigger;
		private Func<float, bool> repeatingTrigger;

		private float elapsed;

		public Timer(float duration, Action<float> trigger, Action<float> tick = null, float elapsed = 0)
		{
			this.trigger = trigger;
			this.tick = tick;
			this.elapsed = elapsed;

			Duration = duration;
		}

		public Timer(float duration, Func<float, bool> repeatingTrigger, Action<float> tick = null, float elapsed = 0)
		{
			this.repeatingTrigger = repeatingTrigger;
			this.tick = tick;
			this.elapsed = elapsed;

			Duration = duration;
		}

		public float Duration { get; set; }

		public bool Paused { get; set; }
		public bool Complete { get; private set; }

		public string Key { get; set; }

		public TimerCollection Collection { get; set; }
		public DoublyLinkedListNode<Timer> Node { get; set; }

		public void Update(float dt)
		{
			if (Paused)
			{
				return;
			}

			elapsed += dt * 1000;

			if (elapsed >= Duration)
			{
				elapsed -= Duration;

				if (trigger != null)
				{
					trigger(elapsed);
					Complete = true;
					Collection?.Remove(this);
				}
				else if (!repeatingTrigger(elapsed))
				{
					Complete = true;
				}
			}

			if (!Complete)
			{
				tick?.Invoke(elapsed / Duration);
			}
		}
	}
}
