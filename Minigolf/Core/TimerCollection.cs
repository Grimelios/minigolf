﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Minigolf.Interfaces;
using Minigolf.Structures;

namespace Minigolf.Core
{
	public class TimerCollection : IDynamic
	{
		private DoublyLinkedList<Timer> timerList;
		private Dictionary<string, DoublyLinkedListNode<Timer>> nodeMap;

		public TimerCollection()
		{
			timerList = new DoublyLinkedList<Timer>();
			nodeMap = new Dictionary<string, DoublyLinkedListNode<Timer>>();
		}

		public void Add(Timer timer, string key = null)
		{
			if (key != null && nodeMap.TryGetValue(key, out DoublyLinkedListNode<Timer> existingNode))
			{
				timerList.Remove(existingNode);
				nodeMap.Remove(key);
			}

			DoublyLinkedListNode<Timer> node = new DoublyLinkedListNode<Timer>(timer);
			timer.Key = key;
			timer.Node = node;
			timer.Collection = this;
			timerList.Add(node);

			if (key != null)
			{
				nodeMap.Add(key, node);
			}
		}

		public void Remove(Timer timer)
		{
			timerList.Remove(timer.Node);

			if (timer.Key != null)
			{
				nodeMap.Remove(timer.Key);
			}
		}

		public void Update(float dt)
		{
			DoublyLinkedListNode<Timer> node = timerList.Head;

			while (node != null)
			{
				node.Data.Update(dt);
				node = node.Next;
			}
		}
	}
}
