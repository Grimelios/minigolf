﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Minigolf.Json;

namespace Minigolf.Core
{
	using SourceMap = Dictionary<string, Rectangle>;

	public class Atlas
	{
		private SourceMap sourceMap;

		public Atlas(string filename)
		{
			sourceMap = JsonUtilities.Deserialize<SourceMap>(filename, false);
		}

		public Rectangle this[string key] => sourceMap[key];
	}
}
