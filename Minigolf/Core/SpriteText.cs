﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Minigolf.Content;
using Minigolf.Graphics;
using OpenTK;

namespace Minigolf.Core
{
	public class SpriteText : Component2D
	{
		private SpriteFont font;

		private string value;

		private Alignments2D alignment;

		public SpriteText(string font, string value, Alignments2D alignment = Alignments2D.Left | Alignments2D.Top) :
			this(ContentLoader.LoadFont(font), value, alignment)
		{
		}

		public SpriteText(SpriteFont font, string value, Alignments2D alignment = Alignments2D.Left | Alignments2D.Top)
		{
			this.font = font;
			this.alignment = alignment;

			Value = value;
		}

		public string Value
		{
			get => value;
			set
			{
				this.value = value;

				if (!string.IsNullOrEmpty(value))
				{
					Point dimensions = font.MeasureString(value);
					Origin = Functions.ComputeOrigin(alignment, dimensions.X, dimensions.Y);
				}
			}
		}

		public Point Measure()
		{
			return value == null ? Point.Empty : font.MeasureString(value);
		}

		public override void Draw(SpriteBatch sb)
		{
			if (value != null)
			{
				sb.DrawString(font,value, Position, Origin, Color);
			}
		}
	}
}
