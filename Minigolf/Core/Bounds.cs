﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Minigolf.Interfaces;
using OpenTK;

namespace Minigolf.Core
{
	[DebuggerDisplay("{X}, {Y}, {Width}, {Height}")]
	public class Bounds
	{
		public static Bounds Enclose<T>(List<T> items) where T : IBoundable2D
		{
			int x = items.Min(i => i.Bounds.Left);
			int y = items.Min(i => i.Bounds.Top);
			int width = items.Max(i => i.Bounds.Right) - x;
			int height = items.Max(i => i.Bounds.Bottom) - y;

			return new Bounds(x, y, width, height);
		}

		public Bounds() : this(0, 0, 0, 0)
		{
		}

		public Bounds(Point dimensions) : this(0, 0, dimensions.X, dimensions.Y)
		{
		}

		public Bounds(int width, int height) : this(0, 0, width, height)
		{
		}

		public Bounds(int x, int y, int width, int height)
		{
			X = x;
			Y = y;
			Width = width;
			Height = height;
		}

		public int X { get; set; }
		public int Y { get; set; }
		public int Width { get; set; }
		public int Height { get; set; }

		public Point Location
		{
			get => new Point(X, Y);
			set
			{
				X = value.X;
				Y = value.Y;
			}
		}

		public Point Center
		{
			get => new Point(X + Width / 2, Y + Height / 2);
			set
			{
				X = value.X - Width / 2;
				Y = value.Y - Height / 2;
			}
		}

		public Point Dimensions
		{
			get => new Point(Width, Height);
			set
			{
				Width = value.X;
				Height = value.Y;
			}
		}

		public int Left
		{
			get => X;
			set => X = value;
		}

		public int Right
		{
			get => X + Width - 1;
			set => X = value - Width + 1;
		}

		public int Top
		{
			get => Y;
			set => Y = value;
		}

		public int Bottom
		{
			get => Y + Height - 1;
			set => Y = value - Height + 1;
		}

		public bool Contains(Point point)
		{
			return point.X >= Left && point.X <= Right && point.Y >= Top && point.Y <= Bottom;
		}

		public bool Contains(Vector2 position)
		{
			return position.X >= Left && position.X <= Right && position.Y >= Top && position.Y <= Bottom;
		}

		public bool Overlaps(Bounds other)
		{
			int dX = Math.Abs(Center.X - other.Center.X);
			int dY = Math.Abs(Center.Y - other.Center.Y);

			return dX <= (Width + other.Width) / 2 && dY <= (Height + other.Height) / 2;
		}
	}
}
