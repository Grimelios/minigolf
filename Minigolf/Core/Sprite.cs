﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Minigolf.Content;
using Minigolf.Graphics;
using OpenTK;

namespace Minigolf.Core
{
	public class Sprite : Component2D
	{
		private Texture2D texture;

		public Sprite(string filename, Alignments2D alignment = Alignments2D.Center) :
			this(ContentLoader.LoadTexture(filename), alignment)
		{
		}

		public Sprite(Texture2D texture, Alignments2D alignment = Alignments2D.Center)
		{
			this.texture = texture;

			Origin = Functions.ComputeOrigin(alignment, texture.Width, texture.Height);
		}

		public override void Draw(SpriteBatch sb)
		{
			sb.Draw(texture, Position, Origin, null, Color, Rotation, Scale);
		}
	}
}
