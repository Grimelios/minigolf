﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Minigolf.Graphics;
using Minigolf.Interfaces;
using OpenTK;

namespace Minigolf.Core
{
	[Flags]
	public enum Alignments2D
	{
		Left = 1,
		Right = 2,
		Top = 4,
		Bottom = 8,
		Center = 0
	}

	public abstract class Component2D : IPositionable2D, IRotatable, IScalable2D, IColorable, IDynamic, IRenderable2D
	{
		public Vector2 Position { get; set; }
		public Vector2 Origin { get; protected set; }
		public Vector2 Scale { get; set; } = Vector2.One;

		public Color Color { get; set; } = Color.White;

		public float Rotation { get; set; }

		public virtual void Update(float dt)
		{
		}

		public virtual void Draw(SpriteBatch sb)
		{
		}
	}
}
