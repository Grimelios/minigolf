﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Minigolf.Graphics;
using Minigolf.Interfaces;
using Minigolf.Structures;
using OpenTK;

namespace Minigolf.Core
{
	public abstract class Container2D : IPositionable2D, IBoundable2D, IDynamic, IRenderable2D
	{
		private Vector2 position;

		protected Container2D()
		{
			TimerCollection = new TimerCollection();
		}

		protected TimerCollection TimerCollection { get; }

		public virtual Vector2 Position
		{
			get => position;
			set
			{
				position = value;

				Point point = value.ToPoint();

				if (Centered)
				{
					Bounds.Center = point;
				}
				else
				{
					Bounds.Location = point;
				}
			}
		}

		public Bounds Bounds { get; protected set; } = new Bounds();

		public int Width
		{
			get => Bounds.Width;
			set => Bounds.Width = value;
		}

		public int Height
		{
			get => Bounds.Height;
			set => Bounds.Height = value;
		}

		public bool Centered { get; set; }
		public bool Visible { get; set; } = true;

		public virtual void Update(float dt)
		{
			TimerCollection.Update(dt);
		}

		public virtual void Draw(SpriteBatch sb)
		{
		}
	}
}
