﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Minigolf.Settings
{
	public class GameplaySettings
	{
		public float MouseSensitivity { get; set; }
		public float ScrollSensitivity { get; set; }

		public bool InvertX { get; set; }
		public bool InvertY { get; set; }
		public bool MiddleDrag { get; set; }
	}
}
