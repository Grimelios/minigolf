﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Minigolf.Settings
{
	public enum WindowModes
	{
		Windowed,
		BorderlessWindowed,
		Fullscreen
	}

	public class VideoSettings
	{
		public float FieldOfView { get; set; }

		public int WindowWidth { get; set; }
		public int WindowHeight { get; set; }

		public WindowModes WindowMode { get; set; }
	}
}
