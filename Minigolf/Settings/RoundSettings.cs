﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Minigolf.Data;

namespace Minigolf.Settings
{
	public enum GameModes
	{
		Race,
		Traditional
	}

	public class RoundSettings
	{
		public string Course { get; set; }

		public int CourseVariation { get; set; }
		public int MinuteLimit { get; set; }
		public int SecondLimit { get; set; }

		public float GravityScale { get; set; }

		public GameModes GameMode { get; set; }
	}
}
