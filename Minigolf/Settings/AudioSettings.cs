﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Minigolf.Settings
{
	public class AudioSettings
	{
		public int MasterVolume { get; set; }
		public int MusicVolume { get; set; }
		public int SoundVolume { get; set; }
	}
}
