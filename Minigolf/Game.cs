﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Minigolf.Core;
using Minigolf.Entities;
using Minigolf.Graphics;
using Minigolf.Input;
using Minigolf.Interfaces;
using Minigolf.Loops;
using Minigolf.Networking;
using Minigolf.Shaders;
using Minigolf.UI;
using Minigolf.UI.Menus;
using Minigolf.UI.Screens;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;

namespace Minigolf
{
	public class Game : GameWindow
	{
		private const int DefaultWidth = 800;
		private const int DefaultHeight = 600;
		private const int ColorDepth = 32;
		private const int DepthBuffer = 24;
		private const int Samples = 4;

		private Camera3D camera;
		private GamePeer gamePeer;
		private GameLoop gameLoop;
		private SpriteBatch spriteBatch;
		private MenuManager menuManager;
		private ScreenManager screenManager;
		private PrimitiveBatch primitiveBatch;
		private InputGenerator inputGenerator;

		private bool firstKeyPress;

		public Game() : base(DefaultWidth, DefaultHeight, new GraphicsMode(ColorDepth, DepthBuffer, 0, Samples))
		{
			firstKeyPress = true;

			Messaging.Subscribe(MessageTypes.Exit, (data, dt) => Exit());
			Messaging.ProcessChanges();
		}

		private void Initialize()
		{
			Title = "Minigolf";
			//CursorVisible = false;

			GL.ClearColor(Color.Black);
			GL.Enable(EnableCap.DepthTest);
			GL.Enable(EnableCap.Blend);
			GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);

			camera = new Camera3D();
			gamePeer = new GamePeer();
			spriteBatch = new SpriteBatch();
			menuManager = new MenuManager();
			screenManager = new ScreenManager();
			primitiveBatch = new PrimitiveBatch(camera);
			inputGenerator = new InputGenerator(this);

			GameLoop[] loops =
			{
				new TitleLoop(),
				new LobbyLoop(),
				new GameplayLoop()
			};

			foreach (GameLoop loop in loops)
			{
				loop.Camera = camera;
				loop.Peer = gamePeer;
				loop.SpriteBatch = spriteBatch;
				loop.MenuManager = menuManager;
				loop.ScreenManager = screenManager;
				loop.PrimitiveBatch = primitiveBatch;
			}

			gameLoop = loops[2];
			gameLoop.Initialize();

			Messaging.ProcessChanges();
		}

		protected override void OnLoad(EventArgs e)
		{
			Initialize();
		}

		protected override void OnResize(EventArgs e)
		{
			Resolution.Resize(Width, Height);
		}

		protected override void OnKeyDown(KeyboardKeyEventArgs e)
		{
			if (firstKeyPress)
			{
				KeyModifiers modifiers = e.Modifiers;

				inputGenerator.Alt = modifiers.HasFlag(KeyModifiers.Alt);
				inputGenerator.Shift = modifiers.HasFlag(KeyModifiers.Shift);
				inputGenerator.Control = modifiers.HasFlag(KeyModifiers.Control);

				firstKeyPress = false;
			}

			if (!e.IsRepeat)
			{
				inputGenerator.OnKeyPress(e.Key);
			}
		}

		protected override void OnKeyUp(KeyboardKeyEventArgs e)
		{
			if (!e.IsRepeat)
			{
				inputGenerator.OnKeyRelease(e.Key);
			}
		}

		protected override void OnUpdateFrame(FrameEventArgs e)
		{
			float dt = (float)e.Time;

			inputGenerator.Update(dt);
			camera.Update(dt);
			gameLoop.Update(dt);

			// This value is used to set modifier flags for keyboard input (alt, shift, and control). Those flags are only set for the
			// first key press each frame (which can technically lead to sub-frame inaccuracy, but that's fine).
			firstKeyPress = true;

			Messaging.ProcessChanges();
		}

		protected override void OnRenderFrame(FrameEventArgs e)
		{
			GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
			
			gameLoop.Draw();
			SwapBuffers();
		}
	}
}
