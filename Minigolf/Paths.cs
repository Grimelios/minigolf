﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Minigolf
{
	public static class Paths
	{
		public const string Content = "Content/";
		public const string Fonts = Content + "Fonts/";
		public const string Json = Content + "Json/";
		public const string Models = Content + "Models/";
		public const string Shaders = Content + "Shaders/";
		public const string Textures = Content + "Textures/";
		public const string Properties = Content + "Properties/";
	}
}
