﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Minigolf.Graphics;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Minigolf.Content
{
	using FontCache = Dictionary<string, SpriteFont>;
	using ModelCache = Dictionary<string, Model>;
	using TextureCache = Dictionary<string, Texture2D>;

	public static class ContentLoader
	{
		private static FontCache fontCache;
		private static FontLoader fontLoader;
		private static ModelCache modelCache;
		private static ModelLoader modelLoader;
		private static TextureCache textureCache;
		private static TextureLoader textureLoader;

		static ContentLoader()
		{
			fontCache = new FontCache();
			fontLoader = new FontLoader();
			modelCache = new ModelCache();
			modelLoader = new ModelLoader();
			textureCache = new TextureCache();
			textureLoader = new TextureLoader();
		}

		public static SpriteFont LoadFont(string name)
		{
			if (!fontCache.TryGetValue(name, out SpriteFont font))
			{
				font = fontLoader.Load(name);
				fontCache.Add(name, font);
			}

			return font;
		}

		public static Model LoadModel(string filename)
		{
			if (!modelCache.TryGetValue(filename, out Model model))
			{
				model = modelLoader.Load(Paths.Models + filename);
				modelCache.Add(filename, model);
			}

			return model;
		}

		public static Texture2D LoadTexture(string filename, string folder = Paths.Textures)
		{
			if (!textureCache.TryGetValue(filename, out Texture2D texture))
			{
				texture = textureLoader.Load(filename, folder);
				textureCache.Add(filename, texture);
			}

			return texture;
		}
	}
}
