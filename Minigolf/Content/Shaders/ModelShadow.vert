﻿#version 330

in vec3 vPosition;
in vec3 vNormal;
in vec4 vColor;

out vec4 fShadowCoords;
out vec4 fColor;

uniform mat4 mvp;
uniform mat3 orientation;
uniform mat4 lightMvp;
uniform vec3 aColor;
uniform vec3 lDirection;
uniform vec3 lColor;

void main()
{
	gl_Position = mvp * vec4(vPosition, 1);

	fShadowCoords = lightMvp * vec4(vPosition, 1);

	float dotValue = dot(lDirection, orientation * -vNormal);

	vec4 ambient = vec4(aColor, 1);
	vec4 diffuse = vec4(max(dotValue, 0) * lColor, 1);

	fColor = vColor * (ambient + diffuse);
}
