﻿#version 330

in vec3 vPosition;

uniform mat4 lightMvp;

void main()
{
	gl_Position = lightMvp * vec4(vPosition, 1);
}
