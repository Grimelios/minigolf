﻿#version 330

in vec2 vPosition;
in vec2 vTexCoords;
in vec2 vQuadCoords;
in vec4 vColor;

out vec4 fColor;
out vec2 fTexCoords;
out vec2 fQuadCoords;

uniform mat4 mvp;
uniform int uHeight;
uniform int uDirection;
uniform float uProgress;

void main()
{
	vec4 position = vec4(vPosition, 0, 1);
	position.y += uHeight * (1 - uProgress) * uDirection;
	position = mvp * position;
	position.y *= -1;

	gl_Position = position;
	fColor = vColor;
	fTexCoords = vTexCoords;
	fQuadCoords = vQuadCoords;
}
