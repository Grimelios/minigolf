﻿#version 330

in vec4 fColor;
in vec4 fShadowCoords;

//uniform sampler2DShadow shadowSampler;
uniform sampler2D shadowSampler;

void main()
{
	vec3 coords = fShadowCoords.xyz / fShadowCoords.w;

	coords = coords * 0.5 + 0.5;

	//float visibility = texture(shadowSampler, coords) == 0 ? 0.25 : 1;
	float depth = texture(shadowSampler, coords.xy).r;

	if (depth > 0)
	{
		gl_FragColor = vec4(vec3(depth), 1);
	}
	else
	{
		gl_FragColor = vec4(vec3(0, 0, 1), fColor.a);
	}

	//gl_FragColor = vec4(coords.xy, 0, fColor.a);
}
