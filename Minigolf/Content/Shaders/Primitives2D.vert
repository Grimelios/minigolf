﻿#version 330

in vec2 vPosition;
in vec4 vColor;

out vec4 fColor;

uniform mat4 mvp;

void main()
{
	vec4 position = mvp * vec4(vPosition, 0, 1);
	position.y *= -1;

	gl_Position = position;
	fColor = vColor;
}
