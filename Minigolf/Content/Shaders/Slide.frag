﻿#version 330

in vec4 fColor;
in vec2 fTexCoords;
in vec2 fQuadCoords;

uniform sampler2D sampler;
uniform float uProgress;
uniform int uDirection;

void main()
{
	bool visible = uDirection == 1 ? fQuadCoords.y <= uProgress : fQuadCoords.y >= 1 - uProgress;
	vec4 color = visible
		? fColor * texture2D(sampler, fTexCoords)
		: vec4(0, 0, 0, 0);

	gl_FragColor = color;
}
