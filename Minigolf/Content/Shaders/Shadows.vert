﻿#version 330

in vec3 vPosition;
in vec3 vNormal;

out vec4 fColor;
out vec4 fShadowPosition;

uniform mat4 mvp;
uniform mat4 lightMatrix;
uniform vec3 aColor;
uniform vec3 lDirection;
uniform vec3 lColor;

void main()
{
	gl_Position = mvp * vec4(vPosition, 1);
	
	vec4 ambient = vec4(aColor, 1);
	vec4 diffuse = vec4(max(dot(lDirection, -vNormal), 0) * lColor, 1);

	fColor = ambient + diffuse;
}
