﻿#version 330

in vec4 fColor;
in vec2 fTexCoords;

uniform sampler2D sampler;

void main()
{
	gl_FragColor = fColor * texture2D(sampler, fTexCoords);
}
