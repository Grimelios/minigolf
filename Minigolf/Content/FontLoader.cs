﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Minigolf.Graphics;
using OpenTK;

namespace Minigolf.Content
{
	public class FontLoader
	{
		private const int CharacterRange = 127;

		public SpriteFont Load(string name)
		{
			string[] lines = File.ReadAllLines(Paths.Fonts + name + ".fnt");

			// Not all entries within the range will have values (only included characters).
			CharacterData[] data = new CharacterData[CharacterRange];
			Texture2D texture = ContentLoader.LoadTexture(name + "_0.png", Paths.Fonts);

			int tWidth = texture.Width;
			int tHeight = texture.Height;

			// The third token on the first line is "size=[value]", but spaces in the font name must be accounted for.
			string firstLine = lines[0];

			int index1 = firstLine.IndexOf("size") + 5;
			int index2 = firstLine.IndexOf(' ', index1);
			int size = int.Parse(firstLine.Substring(index1, index2 - index1));

			// The first four lines of the .fnt file aren't useful.
			int lineIndex = 4;

			while (lineIndex < lines.Length)
			{
				// A line starting with 'k' is a kerning line.
				if (lines[lineIndex][0] == 'k')
				{
					lineIndex++;

					break;
				}

				string[] tokens = lines[lineIndex].Split((char[])null, StringSplitOptions.RemoveEmptyEntries);

				int index = ParseValue(tokens[1]);
				int x = ParseValue(tokens[2]);
				int y = ParseValue(tokens[3]);
				int width = ParseValue(tokens[4]);
				int height = ParseValue(tokens[5]);
				int offsetX = ParseValue(tokens[6]);
				int offsetY = ParseValue(tokens[7]);
				int advance = ParseValue(tokens[8]);

				data[index] = new CharacterData(x, y, width, height, advance, tWidth, tHeight, new Vector2(offsetX, offsetY));
				lineIndex++;
			}

			// Not all character sets have kerning data.
			if (lineIndex != lines.Length)
			{
				ParseKerning(data, lines, lineIndex);
			}

			return new SpriteFont(texture, data, size);
		}

		private static void ParseKerning(CharacterData[] data, string[] lines, int lineIndex)
		{
			int kerningIndex = -1;

			Dictionary<int, int> kerningMap = new Dictionary<int, int>();

			// Kerning data is listed after basic character data.
			while (lineIndex < lines.Length)
			{
				KerningData kerningData = new KerningData(lines[lineIndex]);

				if (kerningData.First != kerningIndex)
				{
					if (kerningIndex != -1)
					{
						data[kerningIndex].KerningMap = kerningMap;
						kerningMap.Clear();
					}

					kerningIndex = kerningData.First;
				}

				kerningMap.Add(kerningData.Second, kerningData.Amount);
				lineIndex++;
			}

			data[kerningIndex].KerningMap = kerningMap;
		}

		private static int ParseValue(string token)
		{
			return int.Parse(token.Split('=')[1]);
		}

		private class KerningData
		{
			public KerningData(string line)
			{
				string[] tokens = line.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

				First = ParseValue(tokens[1]);
				Second = ParseValue(tokens[2]);
				Amount = ParseValue(tokens[3]);
			}

			public int First { get; }
			public int Second { get; }
			public int Amount { get; }
		}
	}
}
