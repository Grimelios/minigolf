﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Minigolf.Graphics;
using OpenTK;

namespace Minigolf.Content
{
	public class ModelLoader
	{
		public Model Load(string filename)
		{
			string[] lines = File.ReadAllLines(filename);

			// The first four lines of .obj files exported from Blender aren't important.
			int lineIndex = 4;

			List<Vector3> vertices = new List<Vector3>();
			List<Vector3> faceNormals = new List<Vector3>();

			Vector3 ParseFunction(string line)
			{
				string[] tokens = line.Split(' ');

				float x = float.Parse(tokens[1]);
				float y = float.Parse(tokens[2]);
				float z = float.Parse(tokens[3]);

				return new Vector3(x, y, z);
			}

			// Parse vertices.
			do
			{
				vertices.Add(ParseFunction(lines[lineIndex]));
				lineIndex++;
			}
			while (lines[lineIndex][1] != 'n');

			// Parse face normals.
			do
			{
				faceNormals.Add(ParseFunction(lines[lineIndex]));
				lineIndex++;
			}
			while (lines[lineIndex][0] == 'v');

			// This progresses the index to face lines.
			lineIndex += 2;

			Vector3[] vertexNormals = new Vector3[vertices.Count];
			List<int> indices = new List<int>();

			// When a quad (or larger shape) is split into triangles, it adds additional face lines to the .obj file. Every triangle adds
			// indices, but for normals, vertices should only pull from each connected face once. This array is used to track those faces.
			HashSet<int>[] faceMap = new HashSet<int>[vertices.Count];

			for (int i = 0; i < vertices.Count; i++)
			{
				faceMap[i] = new HashSet<int>();
			}

			for (int i = lineIndex; i < lines.Length; i++)
			{
				string[] tokens = lines[i].Split(' ');

				for (int j = 1; j < tokens.Length; j++)
				{
					string[] subTokens = tokens[j].Split('/');

					// Face lines have two slashes separating indices, so the middle token is always empty.
					int vIndex = int.Parse(subTokens[0]) - 1;
					int fIndex = int.Parse(subTokens[2]) - 1;

					if (!faceMap[vIndex].Contains(fIndex))
					{
						vertexNormals[vIndex] += faceNormals[fIndex];
						faceMap[vIndex].Add(fIndex);
					}

					indices.Add(vIndex);
				}
			}

			for (int i = 0; i < vertexNormals.Length; i++)
			{
				vertexNormals[i] = Vector3.Normalize(vertexNormals[i]);
			}

			return new Model(vertices.ToArray(), null, vertexNormals, indices.ToArray());
		}
	}
}
