﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Minigolf.Settings;

namespace Minigolf
{
	public static class Resolution
	{
		public const int Width = 1920;
		public const int Height = 1080;

		static Resolution()
		{
			Messaging.Subscribe(MessageTypes.VideoSettings, (data, dt) =>
			{
				VideoSettings settings = (VideoSettings)data;

				WindowWidth = settings.WindowWidth;
				WindowHeight = settings.WindowHeight;
			});
		}

		public static int WindowWidth { get; private set; }
		public static int WindowHeight { get; private set; }

		public static float WindowAspect => (float)WindowWidth / WindowHeight;

		public static void Resize(int windowWidth, int windowHeight)
		{
			WindowWidth = windowWidth;
			WindowHeight = windowHeight;

			Messaging.Send(MessageTypes.Resize, new Point(windowWidth, windowHeight));
		}
	}
}
