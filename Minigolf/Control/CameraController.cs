﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Minigolf.Entities;
using Minigolf.Input.Data;
using Minigolf.Json;
using Minigolf.Settings;
using OpenTK;
using MouseButtons = Minigolf.Input.Data.MouseButtons;

namespace Minigolf.Control
{
	public class CameraController
	{
		private Camera3D camera;
		private CameraControls controls;

		private int invertX;
		private int invertY;

		private float mouseSensitivity;
		private float scrollSensitivity;

		private bool middleDrag;
		private bool reverseDirection;

		public CameraController(Camera3D camera)
		{
			this.camera = camera;

			controls = JsonUtilities.Deserialize<CameraControls>("Controls/CameraControls.json", false);
			
			Messaging.Subscribe(MessageTypes.Input, (data, dt) => HandleInput((AggregateData)data, dt));
			Messaging.Subscribe(MessageTypes.GameplaySettings, (data, dt) =>
			{
				GameplaySettings settings = (GameplaySettings)data;

				InvertX = settings.InvertX;
				InvertY = settings.InvertY;
				middleDrag = settings.MiddleDrag;
				mouseSensitivity = settings.MouseSensitivity;
				scrollSensitivity = settings.ScrollSensitivity;
			});
		}

		public bool InvertX
		{
			get => invertX == -1;
			set => invertX = value ? -1 : 1;
		}

		public bool InvertY
		{
			get => invertY == -1;
			set => invertY = value ? -1 : 1;
		}

		private void HandleInput(AggregateData data, float dt)
		{
			switch (camera.Mode)
			{
				case CameraModes.Fly: ControlFly(data, dt);
					break;

				case CameraModes.Follow: ControlFollow(data);
					break;
			}
		}

		private void ControlFly(AggregateData data, float dt)
		{
			Vector3 direction = Vector3.Transform(-Vector3.UnitZ, camera.Orientation.Inverted());

			bool forward = data.Query(controls.Forward, InputStates.Held);
			bool back = data.Query(controls.Back, InputStates.Held);

			if (forward ^ back)
			{
				camera.Speed += CameraConstants.Acceleration * dt;
				camera.Decelerating = false;
				reverseDirection = back;
			}
			else
			{
				camera.Decelerating = camera.Speed > 0;
			}

			// Camera speed is always positive, but the direction can change.
			if (reverseDirection)
			{
				direction *= -1;
			}

			camera.Direction = direction;

			MouseData mouseData = (MouseData)data[InputTypes.Mouse];

			if (!middleDrag || mouseData.Query(MouseButtons.Middle, InputStates.Held))
			{
				Point delta = mouseData.PositionDelta;

				camera.Yaw += delta.X * mouseSensitivity * invertX;
				camera.Pitch += delta.Y * mouseSensitivity * invertY;
			}
		}

		private void ControlFollow(AggregateData data)
		{
			MouseData mouseData = (MouseData)data[InputTypes.Mouse];

			if (!middleDrag || mouseData.Query(MouseButtons.Middle, InputStates.Held))
			{
				Point delta = mouseData.PositionDelta;

				camera.Yaw -= delta.X * mouseSensitivity * invertX;
				camera.Pitch -= delta.Y * mouseSensitivity * invertY;
			}

			if (mouseData.ScrollDelta != 0)
			{
				camera.FollowDistance -= mouseData.ScrollDelta * scrollSensitivity;
			}
		}
	}
}
