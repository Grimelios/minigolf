﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Minigolf.Input.Data;

namespace Minigolf.Control
{
	public class StrokeController
	{
		private Camera3D camera;

		public StrokeController(Camera3D camera)
		{
			this.camera = camera;

			Messaging.Subscribe(MessageTypes.Mouse, (data, dt) => HandleMouse((MouseData)data));
		}

		private void HandleMouse(MouseData data)
		{
			if (data.Query(MouseButtons.Left, InputStates.PressedThisFrame))
			{
				Messaging.Send(MessageTypes.Stroke, null);
			}
		}
	}
}
