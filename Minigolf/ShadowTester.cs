﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Minigolf.Entities;
using Minigolf.Graphics;
using Minigolf.Interfaces;
using Minigolf.Shaders;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Minigolf
{
	public class ShadowTester : IDynamic
	{
		private const int BufferSize = 1024;

		private Camera3D camera;
		private DebugCube[] cubes;
		private ShaderProgram shadowMapShader;
		private ShaderProgram renderShader;

		private Vector3 lightDirection;
		private Matrix4 lightView;
		private Matrix4 projection;
		private Matrix4 lightProjection;
		private Matrix4 lightVP;

		private int indexBuffer;
		private int frameBuffer;
		private int depthTexture;

		private Vector3[] orientations;
		private Vector3[] angularVelocities;

		public ShadowTester(Camera3D camera)
		{
			this.camera = camera;
			
			shadowMapShader = new ShaderProgram("ShadowMap");
			renderShader = new ShaderProgram("ModelShadow");

			lightDirection = -Vector3.UnitX;
			lightView = Matrix4.LookAt(-lightDirection * 40, Vector3.Zero, Vector3.UnitY);
			lightProjection = Matrix4.CreateOrthographicOffCenter(-40, 40, 30, -30, 0.1f, 100);
			lightVP = lightView * lightProjection;

			projection = Matrix4.CreatePerspectiveFieldOfView(MathHelper.PiOver2, 800f / 600, 0.1f, 1000);

			GenerateCubes();

			GL.GenBuffers(1, out indexBuffer);

			// Initialize frame buffer.
			GL.GenFramebuffers(1, out frameBuffer);
			GL.BindFramebuffer(FramebufferTarget.Framebuffer, frameBuffer);

			// Initialize depth texture.
			GL.GenTextures(1, out depthTexture);
			GL.BindTexture(TextureTarget.Texture2D, depthTexture);
			GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.DepthComponent16, BufferSize, BufferSize, 0,
				PixelFormat.DepthComponent, PixelType.Float, IntPtr.Zero);

			// Set texture parameters.
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Nearest);
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Nearest);
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.ClampToEdge);
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.ClampToEdge);
			//GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureCompareMode, (int)TextureCompareMode.CompareRToTexture);

			// Set frame buffer parameters.
			GL.FramebufferTexture(FramebufferTarget.Framebuffer, FramebufferAttachment.Depth, depthTexture, 0);
			GL.DrawBuffer(DrawBufferMode.None);
			GL.ReadBuffer(ReadBufferMode.None);
			GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
		}

		private void GenerateCubes()
		{
			const int Seed = 69;
			const int CubeCount = 30;

			const float Range = 20;
			const float MinScale = 0.5f;
			const float MaxScale = 2;
			const float MinAngularSpeed = 0;
			const float MaxAngularSpeed = 2;

			Random random = new Random(Seed);

			cubes = new DebugCube[CubeCount];
			orientations = new Vector3[CubeCount];
			angularVelocities = new Vector3[CubeCount];

			float GenerateValue(float range) => (float)random.NextDouble() * range - range / 2;
			float GenerateWithinRange(float min, float max) => (float)random.NextDouble() * (max - min) + min;

			for (int i = 0; i < cubes.Length; i++)
			{
				float x = GenerateValue(Range);
				float y = GenerateValue(Range);
				float z = GenerateValue(Range);
				float scale = GenerateWithinRange(MinScale, MaxScale);

				cubes[i] = new DebugCube
				{
					Position = new Vector3(x, y, z),
					Scale = new Vector3(scale)
				};

				float aX = GenerateWithinRange(MinAngularSpeed, MaxAngularSpeed);
				float aY = GenerateWithinRange(MinAngularSpeed, MaxAngularSpeed);
				float aZ = GenerateWithinRange(MinAngularSpeed, MaxAngularSpeed);

				angularVelocities[i] = new Vector3(aX, aY, aZ);
			}
		}

		public void Update(float dt)
		{
			for (int i = 0; i < cubes.Length; i++)
			{
				//orientations[i] += angularVelocities[i] * dt;

				DebugCube cube = cubes[i];
				Vector3 orientation = orientations[i];

				cube.Orientation =
					Matrix3.CreateRotationX(orientation.X) *
					Matrix3.CreateRotationY(orientation.Y) *
					Matrix3.CreateRotationZ(orientation.Z);

				cube.Update(dt);
			}
		}

		public void Draw()
		{
			CreateShadowMap();

			GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
			GL.Viewport(0, 0, Resolution.WindowWidth, Resolution.WindowHeight);

			foreach (DebugCube cube in cubes)
			{
				DrawCube(cube);
			}
		}

		private void CreateShadowMap()
		{
			GL.BindFramebuffer(FramebufferTarget.Framebuffer, frameBuffer);
			GL.Viewport(0, 0, BufferSize, BufferSize);
			GL.Clear(ClearBufferMask.DepthBufferBit);
			GL.UseProgram(shadowMapShader.ProgramId);

			foreach (DebugCube cube in cubes)
			{
				Matrix4 lightMvp = cube.ModelMatrix * lightVP;

				GL.UniformMatrix4(shadowMapShader.Uniforms["lightMvp"], false, ref lightMvp);

				int[] indices = cube.Model.Indices;

				BindArray(shadowMapShader, "vPosition", cube.Model.Vertices, 3, Vector3.SizeInBytes);

				GL.BindBuffer(BufferTarget.ElementArrayBuffer, indexBuffer);
				GL.BufferData(BufferTarget.ElementArrayBuffer, indices.Length * sizeof(uint), indices, BufferUsageHint.StaticDraw);

				shadowMapShader.EnableVertexAttributeArrays();

				GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
				GL.DrawElements(BeginMode.Triangles, indices.Length, DrawElementsType.UnsignedInt, 0);

				shadowMapShader.DisableVertexAttributeArrays();
			}
		}

		private void DrawCube(DebugCube cube)
		{
			Model model = cube.Model;

			Matrix4 mvp = cube.ModelMatrix * camera.View * projection;
			Matrix4 lightMvp = cube.ModelMatrix * lightVP;
			Matrix3 orientation = cube.Orientation;

			GL.UseProgram(renderShader.ProgramId);
			GL.UniformMatrix4(renderShader.Uniforms["mvp"], false, ref mvp);			
			GL.UniformMatrix4(renderShader.Uniforms["lightMvp"], false, ref lightMvp);
			GL.UniformMatrix3(renderShader.Uniforms["orientation"], false, ref orientation);

			Vector4[] colors = Enumerable.Repeat(cube.Color.ToVector4(), model.Vertices.Length).ToArray();

			BindArray(renderShader, "vPosition", model.Vertices, 3, Vector3.SizeInBytes);
			BindArray(renderShader, "vNormal", model.Normals, 3, Vector3.SizeInBytes);
			BindArray(renderShader, "vColor", colors, 4, Vector4.SizeInBytes);

			// Bind light data.
			GL.Uniform3(renderShader.Uniforms["aColor"], new Vector3(0.05f));
			GL.Uniform3(renderShader.Uniforms["lDirection"], lightDirection);
			GL.Uniform3(renderShader.Uniforms["lColor"], Vector3.One);

			int[] indices = model.Indices;

			// Bind index data.
			GL.BindBuffer(BufferTarget.ElementArrayBuffer, indexBuffer);
			GL.BufferData(BufferTarget.ElementArrayBuffer, indices.Length * sizeof(uint), indices, BufferUsageHint.StaticDraw);

			// Bind shadow map.
			GL.ActiveTexture(TextureUnit.Texture0);
			GL.BindTexture(TextureTarget.Texture2D, frameBuffer);

			renderShader.EnableVertexAttributeArrays();

			GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
			GL.DrawElements(BeginMode.Triangles, indices.Length, DrawElementsType.UnsignedInt, 0);

			renderShader.DisableVertexAttributeArrays();
		}

		private void BindArray<T>(ShaderProgram shader, string key, T[] array, int size, int sizeInBytes) where T : struct
		{
			GL.BindBuffer(BufferTarget.ArrayBuffer, shader.Buffers[key]);
			GL.BufferData(BufferTarget.ArrayBuffer, array.Length * sizeInBytes, array, BufferUsageHint.StaticDraw);
			GL.VertexAttribPointer(shader.Attributes[key], size, VertexAttribPointerType.Float, false, 0, 0);
		}
	}
}
