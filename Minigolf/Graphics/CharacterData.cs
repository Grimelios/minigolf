﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;

namespace Minigolf.Graphics
{
	public class CharacterData
	{
		public CharacterData(int x, int y, int width, int height, int advance, int tWidth, int tHeight, Vector2 offset)
		{
			Width = width;
			Height = height;
			Advance = advance;

			float fX = (float)x / tWidth;
			float fY = (float)y / tHeight;
			float fWidth = (float)width / tWidth;
			float fHeight = (float)height / tHeight;

			// Character source rects can have their UV coordinates pre-computed (within the normalized texture range).
			TexCoords = new[]
			{
				new Vector2(fX, fY),
				new Vector2(fX + fWidth, fY),
				new Vector2(fX + fWidth, fY + fHeight),
				new Vector2(fX, fY + fHeight)
			};

			Offset = offset;
		}

		public int Width { get; }
		public int Height { get; }
		public int Advance { get; }

		public Vector2[] TexCoords { get; }
		public Vector2 Offset { get; }

		public Dictionary<int, int> KerningMap { get; set; }
	}
}
