﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;

namespace Minigolf.Graphics
{
	public class Model
	{
		public Model(Vector3[] vertices, Vector2[] texCoords, Vector3[] normals, int[] indices)
		{
			Vertices = vertices;
			TexCoords = texCoords;
			Normals = normals;
			Indices = indices;
		}

		public Vector3[] Vertices { get; }
		public Vector2[] TexCoords { get; }
		public Vector3[] Normals { get; }

		public int[] Indices { get; }
	}
}
