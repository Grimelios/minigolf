﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Minigolf.Core;
using Minigolf.Graphics;
using Minigolf.Shaders;
using Minigolf.Shapes;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Minigolf.Graphics
{
	public enum BatchModes
	{
		Batch,
		Immediate
	}

	public class SpriteBatch
	{
		private ShaderProgram spriteShader;
		private ShaderProgram primitiveShader;
		private ShaderProgram activeShader;
		private ShaderProgram customShader;

		private Texture2D texture;
		private Matrix4 mvp;

		private List<Vector2> vertexList;
		private List<Vector2> texCoordList;
		private List<Vector2> quadCoordList;
		private List<Vector4> colorList;
		private List<int> indexList;

		private Vector2[] defaultTexCoords;

		private int indexBuffer;
		private int batchCount;

		private bool drawing;
		private bool primitiveMode;
		private bool immediateMode;
		private bool quadCoordsActive;

		private BeginMode mode;

		public SpriteBatch()
		{
			spriteShader = new ShaderProgram("Sprite");
			primitiveShader = new ShaderProgram("Primitives2D");

			defaultTexCoords = new[]
			{
				new Vector2(0, 0),
				new Vector2(1, 0),
				new Vector2(1, 1),
				new Vector2(0, 1)
			};

			GL.GenBuffers(1, out indexBuffer);

			vertexList = new List<Vector2>();
			texCoordList = new List<Vector2>();
			quadCoordList = new List<Vector2>();
			colorList = new List<Vector4>();
			indexList = new List<int>();

			Messaging.Subscribe(MessageTypes.Resize, (data, dt) => OnResize((Point)data));
		}

		private void OnResize(Point data)
		{
			Vector2 halfDimensions = new Vector2(data.X, data.Y) / 2;

			mvp =
				Matrix4.CreateTranslation(new Vector3(-halfDimensions)) *
				Matrix4.CreateScale(new Vector3(1 / halfDimensions.X, 1 / halfDimensions.Y, 0));
		}

		public void Begin(BatchModes mode = BatchModes.Batch)
		{
			if (drawing)
			{
				throw new InvalidOperationException("Must call End() before starting the batch.");
			}

			drawing = true;
			immediateMode = mode == BatchModes.Immediate;
		}

		public void Restart(BatchModes mode)
		{
			if (!drawing)
			{
				throw new InvalidOperationException("Must call Start() before restarting the batch.");
			}

			Flush();
			immediateMode = mode == BatchModes.Immediate;
		}

		public void End()
		{
			if (!drawing)
			{
				throw new InvalidOperationException("Must call Begin() before ending the batch.");
			}

			Flush();
			drawing = false;
			immediateMode = false;
			quadCoordsActive = false;
		}

		public void Apply(ShaderProgram shader)
		{
			Flush();
			customShader = shader;
			quadCoordsActive = shader.Attributes.ContainsKey("vQuadCoords");
		}

		public void Reset()
		{
			Flush();

			activeShader = null;
			customShader = null;
			immediateMode = false;
			quadCoordsActive = false;
		}

		public void Draw(Texture2D texture, Vector2 position, Vector2 origin, Rectangle? sourceRect, Color color, float rotation,
			Vector2 scale)
		{
			VerifySprite(texture);

			Vector2 dimensions = texture.Dimensions * scale;
			Vector2[] vertexData =
			{
				Vector2.Zero,
				new Vector2(dimensions.X, 0),
				dimensions,
				new Vector2(0, dimensions.Y)
			};

			for (int i = 0; i < 4; i++)
			{
				vertexData[i] -= origin;
			}

			if (rotation != 0)
			{
				Quaternion quaternion = Quaternion.FromAxisAngle(Vector3.UnitZ, rotation);

				for (int i = 0; i < vertexData.Length; i++)
				{
					vertexData[i] = Vector2.Transform(vertexData[i], quaternion);
				}
			}

			position = position.ToIntegers();

			for (int i = 0; i < 4; i++)
			{
				vertexData[i] += position;
			}

			vertexList.AddRange(vertexData);
			colorList.AddRange(Enumerable.Repeat(color.ToVector4(), 4));
			texCoordList.AddRange(defaultTexCoords);
			indexList.AddRange(Enumerable.Range(batchCount * 4, 4));
			batchCount++;

			if (immediateMode)
			{
				Flush();
			}
		}

		public void DrawString(SpriteFont font, string value, Vector2 position, Vector2 origin, Color color)
		{
			VerifySprite(font.Texture);

			position -= origin;

			Vector2 dimensions = quadCoordsActive ? font.MeasureString(value).ToVector2() : Vector2.Zero;
			Vector2 localPosition = Vector2.Zero;

			for (int i = 0; i < value.Length; i++)
			{
				int c = value[i];

				CharacterData data = font.Data[c];

				int width = data.Width;
				int height = data.Height;
				int kerning = 0;

				if (i < value.Length - 1 && data.KerningMap != null && data.KerningMap.TryGetValue(value[i + 1], out int amount))
				{
					kerning = amount;
				}

				Vector2 start = data.Offset + new Vector2(kerning, 0);
				Vector2[] vertexData =
				{
					start,
					start + new Vector2(width, 0),
					start + new Vector2(width, height),
					start + new Vector2(0, height)
				};

				vertexList.AddRange(vertexData.Select(v => position + v));
				texCoordList.AddRange(data.TexCoords);
				position.X += data.Advance;
				localPosition.X += data.Advance;

				if (quadCoordsActive)
				{
					quadCoordList.AddRange(vertexData.Select(v => localPosition + Vector2.Divide(v, dimensions)));
				}
			}

			int count = value.Length * 4;

			colorList.AddRange(Enumerable.Repeat(color.ToVector4(), count));
			indexList.AddRange(Enumerable.Range(batchCount * 4, count));
			batchCount += value.Length;

			if (immediateMode)
			{
				Flush();
			}
		}

		public void DrawLine(Vector2 start, Vector2 end, Color color)
		{
			VerifyPrimitive(BeginMode.Lines);

			vertexList.Add(start);
			vertexList.Add(end);
			colorList.AddRange(Enumerable.Repeat(color.ToVector4(), 2));
			indexList.AddRange(Enumerable.Range(batchCount * 2, 2));
			batchCount++;
		}

		public void DrawBounds(Bounds bounds, Color color)
		{
			VerifyPrimitive(BeginMode.Lines);

			Vector2[] vertices =
			{
				new Vector2(bounds.Left, bounds.Top),
				new Vector2(bounds.Right, bounds.Top),
				new Vector2(bounds.Right, bounds.Bottom),
				new Vector2(bounds.Left, bounds.Bottom)
			};

			int[] indices =
			{
				0, 1, 1, 2, 2, 3, 3, 0
			};

			vertexList.AddRange(vertices);
			colorList.AddRange(Enumerable.Repeat(color.ToVector4(), 8));
			indexList.AddRange(indices);
			batchCount += 4;
		}

		public void DrawPrimitives(BeginMode mode, Vector2[] vertexData, Vector4[] colorData, int[] indexData, int batchCount)
		{
			VerifyPrimitive(mode);

			vertexList.AddRange(vertexData);
			colorList.AddRange(colorData);
			indexList.AddRange(indexData);

			this.batchCount += batchCount;
		}

		public void FillBounds(Bounds bounds, Color color)
		{
			VerifyPrimitive(BeginMode.Quads);

			Vector2[] vertices =
			{
				new Vector2(bounds.Left, bounds.Top),
				new Vector2(bounds.Right, bounds.Top),
				new Vector2(bounds.Right, bounds.Bottom),
				new Vector2(bounds.Left, bounds.Bottom)
			};

			vertexList.AddRange(vertices);
			colorList.AddRange(Enumerable.Repeat(color.ToVector4(), 4));
			indexList.AddRange(Enumerable.Range(0, 4));
			batchCount++;
		}

		private void VerifyStart()
		{
			if (!drawing)
			{
				throw new InvalidOperationException("Begin() must be called before drawing.");
			}
		}

		private void VerifySprite(Texture2D texture)
		{
			VerifyStart();

			if (primitiveMode || activeShader == null)
			{
				Flush();

				activeShader = customShader ?? spriteShader;
				mode = BeginMode.Quads;
				primitiveMode = false;
			}

			if (this.texture != texture)
			{
				Flush();

				this.texture = texture;
			}
		}

		private void VerifyPrimitive(BeginMode mode)
		{
			VerifyStart();

			if (!primitiveMode || activeShader == null)
			{
				Flush();

				activeShader = customShader ?? primitiveShader;
				primitiveMode = true;
				texture = null;
			}

			if (this.mode != mode)
			{
				Flush();

				this.mode = mode;
			}
		}

		private void Flush()
		{
			if (batchCount == 0)
			{
				return;
			}

			GL.UseProgram(activeShader.ProgramId);
			GL.UniformMatrix4(activeShader.Uniforms["mvp"], false, ref mvp);

			GL.BindBuffer(BufferTarget.ElementArrayBuffer, indexBuffer);
			GL.BufferData(BufferTarget.ElementArrayBuffer, indexList.Count * sizeof(uint), indexList.ToArray(), BufferUsageHint.StaticDraw);

			BufferArray("vPosition", vertexList, 2, Vector2.SizeInBytes);
			BufferArray("vColor", colorList, 4, Vector4.SizeInBytes);

			if (quadCoordsActive)
			{
				BufferArray("vQuadCoords", quadCoordList, 2, Vector2.SizeInBytes);
			}

			if (!primitiveMode)
			{
				BufferArray("vTexCoords", texCoordList, 2, Vector2.SizeInBytes);

				GL.ActiveTexture(TextureUnit.Texture0);
				GL.BindTexture(TextureTarget.Texture2D, texture.TextureId);
			}

			activeShader.EnableVertexAttributeArrays();

			GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
			GL.DrawElements(mode, indexList.Count, DrawElementsType.UnsignedInt, 0);

			activeShader.DisableVertexAttributeArrays();

			vertexList.Clear();
			colorList.Clear();
			texCoordList.Clear();
			quadCoordList.Clear();
			indexList.Clear();
			batchCount = 0;
		}

		private void BufferArray<T>(string name, List<T> list, int size, int sizeInBytes) where T : struct
		{
			GL.BindBuffer(BufferTarget.ArrayBuffer, activeShader.Buffers[name]);
			GL.BufferData(BufferTarget.ArrayBuffer, list.Count * sizeInBytes, list.ToArray(), BufferUsageHint.StaticDraw);
			GL.VertexAttribPointer(activeShader.Attributes[name], size, VertexAttribPointerType.Float, false, 0, 0);
		}
	}
}
