﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Minigolf.Shaders;
using Minigolf.Shapes;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Minigolf.Graphics
{
	public class PrimitiveBatch
	{
		private const float NearPlane = 0.1f;
		private const float FarPlane = 100;

		private Camera3D camera;
		private ShaderProgram shader;
		private Matrix4 projection;

		private List<Vector3> vertexList;
		private List<Vector4> colorList;
		private List<int> indexList;

		private BeginMode mode;

		private int indexBuffer;
		private int batchCount;
		private int indexOffset;
		private int[] indexArray;

		private bool drawing;

		public PrimitiveBatch(Camera3D camera)
		{
			this.camera = camera;

			shader = new ShaderProgram("Primitives3D");
			projection = Matrix4.CreatePerspectiveFieldOfView(MathHelper.PiOver2, 800f / 600, NearPlane, FarPlane);

			vertexList = new List<Vector3>();
			colorList = new List<Vector4>();
			indexList = new List<int>();

			indexArray = new[]
			{
				// Right face
				0, 1,
				1, 2,
				2, 3,
				3, 0,

				// Left face
				4, 5,
				5, 6,
				6, 7,
				7, 4,

				// Connectors
				0, 4,
				1, 5,
				2, 6,
				3, 7
			};

			GL.GenBuffers(1, out indexBuffer);
		}

		public void Begin()
		{
			if (drawing)
			{
				throw new InvalidOperationException("Must call End() before starting the batch.");
			}

			drawing = true;
		}

		public void End()
		{
			if (!drawing)
			{
				throw new InvalidOperationException("Must call Begin() before ending the batch.");
			}

			Flush();
			drawing = false;
		}
		
		public void DrawBox(Box box, Color color)
		{
			VerifyMode(BeginMode.Lines);

			float halfWidth = box.Width / 2;
			float halfHeight = box.Height / 2;
			float halfDepth = box.Depth / 2;

			Vector3[] vertices =
			{
				new Vector3(halfWidth, halfHeight, halfDepth),
				new Vector3(halfWidth, halfHeight, -halfDepth),
				new Vector3(halfWidth, -halfHeight, -halfDepth),
				new Vector3(halfWidth, -halfHeight, halfDepth),
				new Vector3(-halfWidth, halfHeight, halfDepth),
				new Vector3(-halfWidth, halfHeight, -halfDepth),
				new Vector3(-halfWidth, -halfHeight, -halfDepth),
				new Vector3(-halfWidth, -halfHeight, halfDepth)
			};

			for (int i = 0; i < vertices.Length; i++)
			{
				vertices[i] += box.Position;
			}

			Vector4[] colors = Enumerable.Repeat(color.ToVector4(), 24).ToArray();

			vertexList.AddRange(vertices);
			colorList.AddRange(colors);
			indexList.AddRange(indexArray.Select(i => indexOffset + i));
			batchCount += 12;
			indexOffset += 8;
		}

		public void DrawLine(Vector3 start, Vector3 end, Color color)
		{
			VerifyMode(BeginMode.Lines);
			
			vertexList.Add(start);
			vertexList.Add(end);
			colorList.AddRange(Enumerable.Repeat(color.ToVector4(), 2));
			indexList.AddRange(Enumerable.Range(indexOffset, 2));
			batchCount++;
			indexOffset += 2;
		}

		private void VerifyMode(BeginMode mode)
		{
			if (!drawing)
			{
				throw new InvalidOperationException("Begin() must be called before drawing.");
			}

			if (this.mode != mode)
			{
				Flush();

				this.mode = mode;
			}
		}

		private void Flush()
		{
			if (batchCount == 0)
			{
				return;
			}

			Matrix4 mvp = camera.View * projection;

			GL.UseProgram(shader.ProgramId);
			GL.UniformMatrix4(shader.Uniforms["mvp"], false, ref mvp);

			GL.BindBuffer(BufferTarget.ElementArrayBuffer, indexBuffer);
			GL.BufferData(BufferTarget.ElementArrayBuffer, indexList.Count * sizeof(uint), indexList.ToArray(), BufferUsageHint.StaticDraw);

			BufferArray("vPosition", vertexList, 3, Vector3.SizeInBytes);
			BufferArray("vColor", colorList, 4, Vector4.SizeInBytes);

			shader.EnableVertexAttributeArrays();

			GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
			GL.DrawElements(mode, indexList.Count, DrawElementsType.UnsignedInt, 0);

			shader.DisableVertexAttributeArrays();

			vertexList.Clear();
			colorList.Clear();
			indexList.Clear();
			batchCount = 0;
			indexOffset = 0;
		}

		private void BufferArray<T>(string name, List<T> list, int size, int sizeInBytes) where T : struct
		{
			GL.BindBuffer(BufferTarget.ArrayBuffer, shader.Buffers[name]);
			GL.BufferData(BufferTarget.ArrayBuffer, list.Count * sizeInBytes, list.ToArray(), BufferUsageHint.StaticDraw);
			GL.VertexAttribPointer(shader.Attributes[name], size, VertexAttribPointerType.Float, false, 0, 0);
		}
	}
}
