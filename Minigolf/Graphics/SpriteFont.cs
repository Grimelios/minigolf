﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;

namespace Minigolf.Graphics
{
	public class SpriteFont
	{
		private int size;

		public SpriteFont(Texture2D texture, CharacterData[] data, int size)
		{
			this.size = size;

			Texture = texture;
			Data = data;
		}

		public Texture2D Texture{ get; }
		public CharacterData[] Data { get; }

		public Point MeasureString(string value, bool literal = false)
		{
			// In order to properly measure width, the X offset of the first character should be ignored.
			int width = Data[value[0]].Width;
			int height = size;
			
			for (int i = 1; i < value.Length; i++)
			{
				CharacterData data = Data[value[i]];
				width += (int)data.Offset.X + data.Width;
			}

			// For rendering multiple strings, full line height should be used in order to ensure that all strings line up properly (even
			// if their literal heights are different). For isolated strings, though, it can be useful to compute dimensions exactly.
			if (literal)
			{
				int min = int.MaxValue;
				int max = int.MinValue;

				foreach (char c in value)
				{
					CharacterData data = Data[c];

					min = Math.Min(min, (int)data.Offset.Y);
					max = Math.Max(max, (int)data.Offset.Y + data.Height);
				}

				height = max - min;
			}

			return new Point(width, height);
		}
	}
}
