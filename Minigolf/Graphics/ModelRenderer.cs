﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Minigolf.Entities;
using Minigolf.Shaders;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Minigolf.Graphics
{
	public class ModelRenderer
	{
		private const float NearPlane = 0.1f;
		private const float FarPlane = 100;

		private Camera3D camera;
		private Matrix4 projection;
		private ShaderProgram shader;

		private int indexBuffer;

		public ModelRenderer(Camera3D camera)
		{
			this.camera = camera;

			shader = new ShaderProgram("Model");
			projection = Matrix4.CreatePerspectiveFieldOfView(MathHelper.PiOver2, 800f / 600, NearPlane, FarPlane);

			GL.UseProgram(shader.ProgramId);
			GL.GenBuffers(1, out indexBuffer);
		}
		
		public void Draw(Entity entity)
		{
			Model model = entity.Model;

			Matrix4 modelMatrix = entity.Orientation.ToMatrix4() * Matrix4.CreateTranslation(entity.Position);
			Matrix4 mvp = modelMatrix * camera.View * projection;

			GL.UseProgram(shader.ProgramId);
			GL.UniformMatrix4(shader.Uniforms["mvp"], false, ref mvp);

			Vector4[] colors = Enumerable.Repeat(entity.Color.ToVector4(), model.Vertices.Length).ToArray();
			Matrix4 orientation = entity.Orientation.ToMatrix4();

			GL.UniformMatrix4(shader.Uniforms["orientation"], false, ref orientation);

			BindArray("vPosition", model.Vertices, 3, Vector3.SizeInBytes);
			//BindArray("vTexCoords", model.TexCoords, 2, Vector2.SizeInBytes);
			BindArray("vNormal", model.Normals, 3, Vector3.SizeInBytes);
			BindArray("vColor", colors, 4, Vector4.SizeInBytes);

			Vector3 lDirection = Vector3.Normalize(-Vector3.UnitX);

			// Bind light data.
			GL.Uniform3(shader.Uniforms["aColor"], new Vector3(0.05f));
			GL.Uniform3(shader.Uniforms["lDirection"], lDirection);
			GL.Uniform3(shader.Uniforms["lColor"], Vector3.One);

			int[] indices = model.Indices;

			// Bind index data.
			GL.BindBuffer(BufferTarget.ElementArrayBuffer, indexBuffer);
			GL.BufferData(BufferTarget.ElementArrayBuffer, indices.Length * sizeof(uint), indices, BufferUsageHint.StaticDraw);

			shader.EnableVertexAttributeArrays();

			GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
			GL.DrawElements(BeginMode.Triangles, indices.Length, DrawElementsType.UnsignedInt, 0);

			shader.DisableVertexAttributeArrays();
		}

		private void BindArray<T>(string key, T[] array, int size, int sizeInBytes) where T : struct
		{
			GL.BindBuffer(BufferTarget.ArrayBuffer, shader.Buffers[key]);
			GL.BufferData(BufferTarget.ArrayBuffer, array.Length * sizeInBytes, array, BufferUsageHint.StaticDraw);
			GL.VertexAttribPointer(shader.Attributes[key], size, VertexAttribPointerType.Float, false, 0, 0);
		}
	}
}
