﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using PixelFormat = OpenTK.Graphics.OpenGL.PixelFormat;

namespace Minigolf.Graphics
{

	public class Texture2D
	{
		public Texture2D(int textureId, int width, int height)
		{
			TextureId = textureId;
			Width = width;
			Height = height;
		}

		public int TextureId { get; }
		public int Width { get; }
		public int Height { get; }

		public Vector2 Dimensions => new Vector2(Width, Height);
	}
}
