﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Minigolf.Core;
using Minigolf.Graphics;
using Minigolf.Interfaces;
using OpenTK;

namespace Minigolf.UI.Menus
{
	public class MenuItem : Container2D, ISelectable
	{
		private Menu parent;
		private SpriteText spriteText;

		public MenuItem(SpriteFont font, string value, int index, Menu parent)
		{
			this.parent = parent;

			Index = index;
			spriteText = new SpriteText(font, value);
			Bounds = new Bounds(font.MeasureString(value));
		}

		public override Vector2 Position
		{
			get => base.Position;
			set
			{
				spriteText.Position = value;

				base.Position = value;
			}
		}

		public int Index { get; }

		public bool Enabled { get; set; } = true;

		public void OnHover()
		{
			parent.SelectedIndex = Index;
		}

		public void OnUnhover()
		{
		}

		public void OnHighlight()
		{
		}

		public void OnUnhighlight()
		{
		}

		public bool OnSelect()
		{
			parent.Submit(Index);

			return false;
		}

		public override void Draw(SpriteBatch sb)
		{
			spriteText.Draw(sb);
		}
	}
}
