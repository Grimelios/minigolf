﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Minigolf.Graphics;
using Minigolf.Input.Data;
using Minigolf.Interfaces;
using OpenTK;

namespace Minigolf.UI.Menus
{
	public class MenuManager : IDynamic, IRenderable2D
	{
		private Menu[] menus;
		private Menu activeMenu;

		public MenuManager()
		{
			menus = new Menu[]
			{
				new TitleMenu()
			};

			foreach (Menu menu in menus)
			{
				menu.Parent = this;
			}

			activeMenu = menus[0];
			activeMenu.Position = new Vector2(100);

			Messaging.Subscribe(MessageTypes.Input, (data, dt) => activeMenu.HandleInput((AggregateData)data));
		}

		public bool Return()
		{
			return false;
		}

		public void Update(float dt)
		{
			activeMenu.Update(dt);
		}

		public void Draw(SpriteBatch sb)
		{
			activeMenu.Draw(sb);
		}
	}
}
