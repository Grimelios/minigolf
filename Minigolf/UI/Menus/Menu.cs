﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Minigolf.Core;
using Minigolf.Graphics;
using Minigolf.Input.Data;
using Minigolf.Input.Sets;
using OpenTK;

namespace Minigolf.UI.Menus
{
	public abstract class Menu : Container2D
	{
		private const int Spacing = 40;
		private const int PointerOffset = 30;

		private static MenuControls controls = new MenuControls();

		private Sprite pointer;
		private MenuItem[] items;
		private SelectableSet<MenuItem> itemSet;

		private int selectedIndex;

		protected Menu()
		{
			pointer = new Sprite("UI/Pointer.png");
		}

		public override Vector2 Position
		{
			get => base.Position;
			set
			{
				Functions.PositionItems(items, value, new Vector2(0, Spacing));

				pointer.Position = items[selectedIndex].Position - new Vector2(PointerOffset, 0);

				base.Position = value;
			}
		}

		public MenuManager Parent { get; set; }

		protected void Initialize(MenuItem[] items)
		{
			this.items = items;

			itemSet = new SelectableSet<MenuItem>(items);
		}

		public int SelectedIndex
		{
			set
			{
				selectedIndex = value;

				Vector2 start = pointer.Position;
				Vector2 end = items[value].Position - new Vector2(PointerOffset, 0);
				Timer timer = Smoothers.CreatePositionTimer(pointer, start, end, 100, EaseTypes.QuadraticOut);

				TimerCollection.Add(timer, "Pointer");
			}
		}

		public void HandleInput(AggregateData data)
		{
			if (data.Query(controls.Return, InputStates.PressedThisFrame))
			{
				// True is returned if the current menu was actually exited (i.e. you're not already at the lowest-level menu).
				if (Parent.Return())
				{
					return;
				}
			}

			if (data.Query(controls.Submit, InputStates.PressedThisFrame))
			{
				Submit(selectedIndex);

				return;
			}

			HandleMouse((MouseData)data[InputTypes.Mouse]);

			bool up = data.Query(controls.Up, InputStates.PressedThisFrame);
			bool down = data.Query(controls.Down, InputStates.PressedThisFrame);

			if (up ^ down)
			{
				items[selectedIndex].OnUnhighlight();

				if (up)
				{
					SelectedIndex = selectedIndex == 0 ? items.Length - 1 : --selectedIndex;
				}
				else
				{
					SelectedIndex = selectedIndex == items.Length - 1 ? 0 : ++selectedIndex;
				}

				items[selectedIndex].OnHighlight();
			}
		}

		private void HandleMouse(MouseData data)
		{
			// The set must be updated even if the mouse doesn't move, since players can still click to select items.
			itemSet.HandleMouse(data);

			if (data.Moved)
			{
				MenuItem hovered = itemSet.HoveredItem;

				if (hovered != null && hovered.Index != selectedIndex)
				{
					items[selectedIndex].OnUnhighlight();
					SelectedIndex = hovered.Index;
					items[selectedIndex].OnHighlight();
				}
			}
		}

		public abstract void Submit(int index);

		public override void Update(float dt)
		{
			foreach (MenuItem item in items)
			{
				item.Update(dt);
			}

			base.Update(dt);
		}

		public override void Draw(SpriteBatch sb)
		{
			pointer.Draw(sb);

			foreach (MenuItem item in items)
			{
				item.Draw(sb);
			}
		}
	}
}
