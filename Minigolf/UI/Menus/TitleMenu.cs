﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Minigolf.Content;
using Minigolf.Graphics;

namespace Minigolf.UI.Menus
{
	public class TitleMenu : Menu
	{
		public TitleMenu()
		{
			string[] nameArray =
			{
				"Create Lobby",
				"Join Lobby",
				"Customize",
				"Settings",
				"Exit"
			};

			SpriteFont font = ContentLoader.LoadFont("Default");
			MenuItem[] items = new MenuItem[nameArray.Length];

			for (int i = 0; i < items.Length; i++)
			{
				items[i] = new MenuItem(font, nameArray[i], i, this);
			}

			Initialize(items);
		}

		public override void Submit(int index)
		{
			switch (index)
			{
				// Create Lobby
				case 0:
					break;

				// Join Lobby
				case 1:
					break;

				// Customize
				case 2:
					break;

				// Settings
				case 3:
					break;
				
				// Exit
				case 4: Messaging.Send(MessageTypes.Exit, null);
					break;
			}
		}
	}
}
