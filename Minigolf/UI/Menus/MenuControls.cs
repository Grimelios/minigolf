﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Minigolf.Input;
using Minigolf.Input.Data;
using OpenTK.Input;

namespace Minigolf.UI.Menus
{
	public class MenuControls
	{
		public MenuControls()
		{
			Up = new List<InputBind>
			{
				new InputBind(InputTypes.Keyboard, Key.W),
				new InputBind(InputTypes.Keyboard, Key.Up)
			};

			Down = new List<InputBind>
			{
				new InputBind(InputTypes.Keyboard, Key.S),
				new InputBind(InputTypes.Keyboard, Key.Down)
			};

			Left = new List<InputBind>
			{
				new InputBind(InputTypes.Keyboard, Key.A),
				new InputBind(InputTypes.Keyboard, Key.Left)
			};

			Right = new List<InputBind>
			{
				new InputBind(InputTypes.Keyboard, Key.D),
				new InputBind(InputTypes.Keyboard, Key.Right)
			};

			Submit = new List<InputBind>
			{
				new InputBind(InputTypes.Keyboard, Key.Enter),
				new InputBind(InputTypes.Keyboard, Key.KeypadEnter),
				new InputBind(InputTypes.Keyboard, Key.Space)
			};

			Return = new List<InputBind>
			{
				new InputBind(InputTypes.Keyboard, Key.Escape),
				new InputBind(InputTypes.Keyboard, Key.Back)
			};
		}

		public List<InputBind> Up { get; }
		public List<InputBind> Down { get; }
		public List<InputBind> Left { get; }
		public List<InputBind> Right { get; }
		public List<InputBind> Submit { get; }
		public List<InputBind> Return { get; }
	}
}
