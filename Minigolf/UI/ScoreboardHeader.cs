﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Minigolf.Content;
using Minigolf.Core;
using Minigolf.Graphics;
using OpenTK;

namespace Minigolf.UI
{
	public class ScoreboardHeader : Container2D
	{
		private SpriteText header;
		private SpriteText[] values;
		private SpriteText footer;

		public ScoreboardHeader(string header, int[] values, string footer)
		{
			SpriteFont font = ContentLoader.LoadFont("Default");

			this.header = new SpriteText(font, header);
			this.footer = new SpriteText(font, footer);
			this.values = new SpriteText[values.Length];

			for (int i = 0; i < values.Length; i++)
			{
				this.values[i] = new SpriteText(font, values[i].ToString());
			}
		}

		public override Vector2 Position
		{
			get => base.Position;
			set { base.Position = value; }
		}

		public override void Draw(SpriteBatch sb)
		{
			header.Draw(sb);
			footer.Draw(sb);

			foreach (SpriteText value in values)
			{
				value.Draw(sb);
			}
		}
	}
}
