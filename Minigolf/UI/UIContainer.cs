﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Minigolf.Core;
using Minigolf.Graphics;
using Minigolf.Interfaces;
using Minigolf.Json;
using OpenTK;

namespace Minigolf.UI
{
	public abstract class UIContainer : IDynamic, IRenderable2D
	{
		protected UIContainer(string jsonFile)
		{
			// Elements are positioned through a window resize message.
			Elements = JsonUtilities.Deserialize<UIElement[]>("UI/" + jsonFile, true);

			Messaging.Subscribe(MessageTypes.Resize, (data, dt) => OnResize((Point)data));
		}

		protected UIElement[] Elements { get; }

		private void OnResize(Point dimensions)
		{
			int width = dimensions.X;
			int height = dimensions.Y;

			foreach (UIElement element in Elements)
			{
				Alignments2D alignment = element.Alignment;

				bool left = (alignment & Alignments2D.Left) == Alignments2D.Left;
				bool right = (alignment & Alignments2D.Right) == Alignments2D.Right;
				bool top = (alignment & Alignments2D.Top) == Alignments2D.Top;
				bool bottom = (alignment & Alignments2D.Bottom) == Alignments2D.Bottom;

				int offsetX = element.OffsetX;
				int offsetY = element.OffsetY;
				int x = left ? offsetX : (right ? width - offsetX : width / 2 + offsetX);
				int y = top ? offsetY : (bottom ? height - offsetY : height / 2 + offsetY);

				element.Position = new Vector2(x, y);
			}
		}

		public T GetElement<T>() where T : UIElement
		{
			return (T)Elements.FirstOrDefault(t => t is T);
		}

		public void Update(float dt)
		{
			foreach (UIElement element in Elements)
			{
				if (element.Visible)
				{
					element.Update(dt);
				}
			}
		}

		public void Draw(SpriteBatch sb)
		{
			foreach (UIElement element in Elements)
			{
				if (element.Visible)
				{
					element.Draw(sb);
				}
			}
		}
	}
}
