﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Minigolf.Core;
using Newtonsoft.Json;

namespace Minigolf.UI
{
	public abstract class UIElement : Container2D
	{
		public Alignments2D Alignment { get; set; }
		
		public int OffsetX { get; set; }
		public int OffsetY { get; set; }
	}
}
