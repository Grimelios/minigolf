﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Minigolf.Graphics;
using Minigolf.Interfaces;

namespace Minigolf.UI
{
	public class ResetTransition : IDynamic, IRenderable2D
	{
		public ResetTransition()
		{
			Messaging.Subscribe(MessageTypes.BallReset, (data, dt) => Sweep());
		}

		private void Sweep()
		{
		}

		public void Update(float dt)
		{
		}

		public void Draw(SpriteBatch sb)
		{
		}
	}
}
