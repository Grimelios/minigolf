﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Minigolf.Core;
using Minigolf.Settings;

namespace Minigolf.UI
{
	public class Scoreboard : Container2D
	{
		public Scoreboard()
		{
			Messaging.Subscribe(MessageTypes.RoundStart, (data, dt) => OnRoundStart((RoundSettings)data));
		}

		private void OnRoundStart(RoundSettings settings)
		{
		}
	}
}
