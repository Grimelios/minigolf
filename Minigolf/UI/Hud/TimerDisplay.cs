﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Minigolf.Core;
using Minigolf.Data;
using Minigolf.Graphics;
using Minigolf.Settings;
using OpenTK;

namespace Minigolf.UI.Hud
{
	using SystemTimer = System.Windows.Forms.Timer;

	public class TimerDisplay : HudElement
	{
		private SystemTimer timer;
		private SpriteText spriteText;

		private int minuteLimit;
		private int secondLimit;
		private int minutes;
		private int seconds;

		public TimerDisplay()
		{
			spriteText = new SpriteText("Timer", null, Alignments2D.Center);
			Visible = false;

			Messaging.Subscribe(MessageTypes.RoundStart, (data, dt) => OnRoundStart((RoundSettings)data));
			Messaging.Subscribe(MessageTypes.HoleStart, (data, dt) => OnHoleStart());
		}

		public override Vector2 Position
		{
			get => base.Position;
			set
			{
				spriteText.Position = value;

				base.Position = value;
			}
		}

		private void OnRoundStart(RoundSettings settings)
		{
			minuteLimit = settings.MinuteLimit;
			secondLimit = settings.SecondLimit;
		}

		private void OnHoleStart()
		{
			minutes = minuteLimit;
			seconds = secondLimit;

			timer?.Stop();
			timer = new SystemTimer();
			timer.Tick += (sender, e) => OnTick();
			timer.Interval = 1000;
			timer.Start();

			spriteText.Value = $"{minutes}:{seconds:D2}";
			Visible = true;

			TimerCollection.Add(Smoothers.CreateFade(spriteText, UIConstants.FadeTime, FadeTypes.FadeIn, EaseTypes.Linear));
		}

		private void OnTick()
		{
			seconds--;

			if (seconds == -1)
			{
				minutes--;
				seconds = 59;
			}

			spriteText.Value = $"{minutes}:{seconds:D2}";
		}

		public override void Draw(SpriteBatch sb)
		{
			spriteText.Draw(sb);
		}
	}
}
