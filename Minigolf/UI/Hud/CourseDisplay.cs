﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Minigolf.Core;
using Minigolf.Data;
using Minigolf.Graphics;
using Minigolf.Settings;
using OpenTK;

namespace Minigolf.UI.Hud
{
	public class CourseDisplay : HudElement
	{
		private SpriteText spriteText;

		public CourseDisplay()
		{
			spriteText = new SpriteText("Default", null, Alignments2D.Center);
			Visible = false;

			Messaging.Subscribe(MessageTypes.RoundStart, (data, dt) =>
			{
				RoundSettings settings = (RoundSettings)data;

				spriteText.Value = settings.Course;
			});
		}

		public override Vector2 Position
		{
			get => base.Position;
			set
			{
				spriteText.Position = value;

				base.Position = value;
			}
		}

		public override void Draw(SpriteBatch sb)
		{
			spriteText.Draw(sb);
		}
	}
}
