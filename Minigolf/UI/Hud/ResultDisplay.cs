﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Minigolf.Core;
using Minigolf.Data;

namespace Minigolf.UI.Hud
{
	public class ResultDisplay : HudElement
	{
		private SpriteText spriteText;
		private Color[] colors;

		private int par;

		private string[] titles;

		public ResultDisplay()
		{
			titles = new[]
			{
				"Condor",
				"Albatross",
				"Eagle",
				"Birdie",
				"Par",
				"Bogey",
				"Double Bogey",
				"Triple Bogey"
			};

			colors = new[]
			{
				Color.Gold,
				Color.OrangeRed,
				Color.Crimson,
				Color.DeepSkyBlue,
				Color.White,
				Color.IndianRed
			};

			spriteText = new SpriteText("Result", null, Alignments2D.Center);

			Messaging.Subscribe(MessageTypes.HoleStart, (data, dt) => OnHoleStart((HoleData)data));
			Messaging.Subscribe(MessageTypes.HoleEnd, (data, dt) => OnHoleEnd((int)data));
		}

		private void OnHoleStart(HoleData data)
		{
			par = data.Par;
		}

		private void OnHoleEnd(int strokes)
		{
			if (strokes == 1)
			{
				spriteText.Value = "Hole in One";

				return;
			}

			int delta = strokes - par;

			// The "Condor" title is only possible with par-6 or higher holes (since getting 1 on a par-5 is covered by hole-in-one).
			if (delta <= 3)
			{
				spriteText.Value = titles[delta + 4];
				spriteText.Color = colors[Math.Min(delta + 4, 5)];
			}
			else
			{
				spriteText.Value = "Par + " + delta;
				spriteText.Color = colors.Last();
			}
		}
	}
}
