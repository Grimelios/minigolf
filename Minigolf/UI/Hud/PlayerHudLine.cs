﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Minigolf.Content;
using Minigolf.Core;
using Minigolf.Graphics;
using OpenTK;

namespace Minigolf.UI.Hud
{
	public class PlayerHudLine : Container2D
	{
		private const int Spacing = 30;

		private SpriteText playerName;
		private SpriteText playerStrokes;

		public PlayerHudLine(string name)
		{
			SpriteFont font = ContentLoader.LoadFont("Default");

			playerName = new SpriteText(font, name, Alignments2D.Left);
			playerStrokes = new SpriteText(font, null, Alignments2D.Center);
		}

		public override Vector2 Position
		{
			get => base.Position;
			set
			{
				playerStrokes.Position = value;
				playerName.Position = value + new Vector2(Spacing, 0);

				base.Position = value;
			}
		}

		public string Name
		{
			set => playerName.Value = value.ToString();
		}

		public int Strokes
		{
			set => playerStrokes.Value = value.ToString();
		}

		public override void Draw(SpriteBatch sb)
		{
			playerName.Draw(sb);
			playerStrokes.Draw(sb);
		}
	}
}
