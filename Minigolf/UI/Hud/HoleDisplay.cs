﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Minigolf.Content;
using Minigolf.Core;
using Minigolf.Data;
using Minigolf.Graphics;
using Minigolf.Interfaces;
using Minigolf.Json;
using Minigolf.Shaders;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Minigolf.UI.Hud
{
	public class HoleDisplay : HudElement
	{
		private SpriteText line1;
		private SpriteText line2;
		private LineData[] lines;
		private Divider divider;
		private HoleDisplayData data;
		private ShaderProgram shader;

		private int lineIndex;
		private int uHeight;
		private int uDirection;
		private int uProgress;

		public HoleDisplay()
		{
			SpriteFont font1 = ContentLoader.LoadFont("Hole1");
			SpriteFont font2 = ContentLoader.LoadFont("Hole2");

			line1 = new SpriteText(font1, null, Alignments2D.Center);
			line2 = new SpriteText(font2, null, Alignments2D.Center);
			data = JsonUtilities.Deserialize<HoleDisplayData>("Data/HoleDisplayData.json", false);
			divider = new Divider(data.DividerWidth, data.DividerHeight);
			shader = new ShaderProgram("Slide");

			uHeight = shader.Uniforms["uHeight"];
			uDirection = shader.Uniforms["uDirection"];
			uProgress = shader.Uniforms["uProgress"];

			lines = new []
			{
				new LineData(font1.MeasureString("H").Y, 1, data.Line1Time),
				new LineData(font2.MeasureString("H").Y, -1, data.Line2Time),
			};

			Visible = false;

			Messaging.Subscribe(MessageTypes.HoleStart, (data, dt) => OnHoleStart((HoleData)data));
		}

		public override Vector2 Position
		{
			get => base.Position;
			set
			{
				line1.Position = value;
				line2.Position = value + new Vector2(0, data.Line2Offset);
				divider.Position = value + new Vector2(0, data.DividerOffset);

				base.Position = value;
			}
		}

		private void OnHoleStart(HoleData data)
		{
			line1.Value = "Hole " + data.Number;
			line2.Value = data.Name;

			Color color = Color.White;

			lines[0].Visible = false;
			lines[1].Visible = false;
			line1.Color = color;
			line2.Color = color;
			divider.Color = color;
			Visible = true;
			lineIndex = 0;

			TimerCollection.Add(Smoothers.CreateScale(divider, Vector2.UnitY, Vector2.One, this.data.DividerTime, EaseTypes.CubicOut));
			TimerCollection.Add(new Timer(this.data.Line1Delay, time => TriggerLine()));
			TimerCollection.Add(new Timer(this.data.Line2Delay, time => TriggerLine()));
			TimerCollection.Add(new Timer(this.data.Lifetime, time => FadeOut()));
		}

		private void TriggerLine()
		{
			LineData lineData = lines[lineIndex];

			lineData.Visible = true;
			TimerCollection.Add(new Timer(lineData.Time,
				time => { },
				progress => lineData.Progress = Easing.Ease(progress, EaseTypes.CubicOut)));

			lineIndex++;
		}

		private void FadeOut()
		{
			Timer t = new Timer(UIConstants.FadeTime,
				time => { Visible = false; },
				progress =>
				{
					Color color = Functions.Lerp(Color.White, Color.Transparent, progress);

					line1.Color = color;
					line2.Color = color;
					divider.Color = color;
				});

			TimerCollection.Add(t);
		}

		public override void Draw(SpriteBatch sb)
		{
			sb.FillBounds(divider.Bounds, divider.Color);

			LineData l1 = lines[0];
			LineData l2 = lines[1];

			if (l1.Visible || l2.Visible)
			{
				sb.Restart(BatchModes.Immediate);
				sb.Apply(shader);

				if (l1.Visible)
				{
					DrawLine(sb, l1, line1);
				}

				if (l2.Visible)
				{
					DrawLine(sb, l2, line2);
				}

				sb.Reset();
			}
		}

		private void DrawLine(SpriteBatch sb, LineData data, SpriteText line)
		{
			GL.UseProgram(shader.ProgramId);
			GL.Uniform1(uHeight, data.Height);
			GL.Uniform1(uDirection, data.Direction);
			GL.Uniform1(uProgress, data.Progress);

			line.Draw(sb);
		}

		private class Divider : IPositionable2D, IScalable2D, IBoundable2D, IColorable
		{
			private Vector2 position;

			private int fullWidth;

			public Divider(int width, int height)
			{
				fullWidth = width;
				Bounds = new Bounds(0, height);
			}

			public Bounds Bounds { get; }

			public Vector2 Position
			{
				get => throw new NotImplementedException();
				set
				{
					position = value;
					Bounds.Center = value.ToPoint();
				}
			}

			public Vector2 Scale
			{
				get => throw new NotImplementedException();
				set
				{
					Bounds.Width = (int)(value.X * fullWidth);
					Bounds.Center = position.ToPoint();
				}
			}

			public Color Color { get; set; }
		}

		private class LineData
		{
			public LineData(int height, int direction, int time)
			{
				Height = height;
				Direction = direction;
				Time = time;
			}

			public bool Visible { get; set; }

			public int Height { get; }
			public int Direction { get; }
			public int Time { get; }

			public float Progress { get; set; }
		}

		private class HoleDisplayData
		{
			public int Line1Delay { get; set; }
			public int Line1Time { get; set; }
			public int Line2Delay { get; set; }
			public int Line2Time { get; set; }
			public int Line2Offset { get; set; }

			public int DividerTime { get; set; }
			public int DividerWidth { get; set; }
			public int DividerHeight { get; set; }
			public int DividerOffset { get; set; }

			public int Lifetime { get; set; }
		}
	}
}
