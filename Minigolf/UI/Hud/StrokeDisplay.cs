﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Minigolf.Core;
using Minigolf.Graphics;
using OpenTK;

namespace Minigolf.UI.Hud
{
	public class StrokeDisplay : HudElement
	{
		private SpriteText spriteText;

		private int strokeCount;

		public StrokeDisplay()
		{
			spriteText = new SpriteText("Stroke", null, Alignments2D.Center);
			Centered = true;
			Visible = false;

			Messaging.Subscribe(MessageTypes.Stroke, (data, dt) =>
			{
				strokeCount++;
				spriteText.Value = strokeCount.ToString();
			});

			Messaging.Subscribe(MessageTypes.HoleStart, (data, dt) =>
			{
				strokeCount = 0;
				spriteText.Value = "0";
				Visible = true;
				TimerCollection.Add(Smoothers.CreateFade(spriteText, UIConstants.FadeTime, FadeTypes.FadeIn, EaseTypes.Linear));
			});
		}

		public override Vector2 Position
		{
			get => base.Position;
			set
			{
				spriteText.Position = value;

				base.Position = value;
			}
		}

		public override void Draw(SpriteBatch sb)
		{
			spriteText.Draw(sb);
		}
	}
}
