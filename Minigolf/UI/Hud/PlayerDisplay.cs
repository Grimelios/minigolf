﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Minigolf.Graphics;
using OpenTK;

namespace Minigolf.UI.Hud
{
	public class PlayerDisplay : HudElement
	{
		private const int Spacing = 40;

		private List<PlayerHudLine> lines;

		public PlayerDisplay()
		{
			lines = new List<PlayerHudLine>();
			lines.Add(new PlayerHudLine("Grimelios"));
		}

		public override Vector2 Position
		{
			get => base.Position;
			set
			{
				Functions.PositionItems(lines, value, new Vector2(0, Spacing));

				base.Position = value;
			}
		}

		public override void Draw(SpriteBatch sb)
		{
			lines.ForEach(l => l.Draw(sb));
		}
	}
}
