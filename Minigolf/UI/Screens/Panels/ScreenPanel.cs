﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Minigolf.Core;
using Minigolf.Graphics;
using Minigolf.Input.Controls;

namespace Minigolf.UI.Screens.Panels
{
	public class ScreenPanel : UIElement
	{
		public List<InputControl> Controls { get; } = new List<InputControl>();

		public override void Draw(SpriteBatch sb)
		{
			Controls.ForEach(c => c.Draw(sb));
		}
	}
}
