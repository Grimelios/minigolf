﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Minigolf.Input.Controls;
using Minigolf.Input.Sets;
using Minigolf.Input.Validation;
using Minigolf.Networking;
using OpenTK;

namespace Minigolf.UI.Screens.Panels
{
	public class ConnectionPanel : ScreenPanel
	{
		private const int PortLength = 5;

		private Textbox ipTextbox;
		private Textbox portTextbox;
		private Button connectButton;

		public ConnectionPanel()
		{
			ipTextbox = new Textbox(150, 30, Submit, "IP Address");
			ipTextbox.Validators.Add(TextboxValidators.IPAddress);

			portTextbox = new Textbox(150, 30, Submit, "Port");
			portTextbox.Validators.Add(TextboxValidators.Length(PortLength));
			portTextbox.Validators.Add(TextboxValidators.Numeric);

			connectButton = new Button("Connect", 100, 40, Submit);

			Controls.Add(ipTextbox);
			Controls.Add(portTextbox);
			Controls.Add(connectButton);
		}

		public override Vector2 Position
		{
			get => base.Position;
			set
			{
				ipTextbox.Position = value;
				portTextbox.Position = value + new Vector2(160, 0);
				connectButton.Position = value + new Vector2(320, 0);

				base.Position = value;
			}
		}

		private void Submit()
		{
			if (ipTextbox.Submit(out object address) && portTextbox.Submit(out object port))
			{
				connectButton.Text = "Cancel";

				Messaging.Send(MessageTypes.Connect, new ConnectionData((IPAddress)address, (int)port));
			}
		}
	}
}
