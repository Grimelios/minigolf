﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Minigolf.Core;
using Minigolf.Graphics;
using OpenTK;

namespace Minigolf.UI.Screens.Panels
{
	public class PlayerLine : Container2D
	{
		private SpriteText playerName;

		public PlayerLine(string name)
		{
			playerName = new SpriteText("Default", name);
		}

		public override Vector2 Position
		{
			get => base.Position;
			set
			{
				playerName.Position = value;

				base.Position = value;
			}
		}

		public override void Draw(SpriteBatch sb)
		{
			playerName.Draw(sb);
		}
	}
}
