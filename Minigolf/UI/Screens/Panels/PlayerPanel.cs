﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Minigolf.Graphics;

namespace Minigolf.UI.Screens.Panels
{
	public class PlayerPanel : ScreenPanel
	{
		private const int LineSpacing = 40;

		private List<PlayerLine> playerLines;

		public PlayerPanel()
		{
			playerLines = new List<PlayerLine>();
			playerLines.Add(new PlayerLine("Grimelios"));
		}

		public override void Draw(SpriteBatch sb)
		{
			playerLines.ForEach(l => l.Draw(sb));
		}
	}
}
