﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Minigolf.Graphics;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Minigolf.UI.Screens.Panels
{
	public class ColorPanel : ScreenPanel
	{
		private const int Diagonal = 40;
		private const int ColorCount = 12;

		private Vector2[] outerVertexData;
		private Vector4[] outerColorData;
		private Vector2[] innerVertexData;
		private Vector4[] innerColorData;

		private int[] outerIndexData;
		private int[] innerIndexData;

		public override Vector2 Position
		{
			get => base.Position;
			set
			{
				RecomputeData(value);

				base.Position = value;
			}
		}

		private void RecomputeData(Vector2 center)
		{
			const int DoubleDiagonal = Diagonal * 2;
			const int TripleDiagonal = Diagonal * 3;
			const int QuadrupleDiagonal = Diagonal * 4;

			// These points start at the bottom and move counter-clockwise.
			Vector2[] diamondData =
			{
				Vector2.Zero,
				new Vector2(Diagonal, -Diagonal),
				new Vector2(0, -DoubleDiagonal),
				new Vector2(-Diagonal, -Diagonal)
			};

			// Manually specifying these locations felt easier than trying to write a function to compute them. Locations start at the top
			// and move clockwise.
			Vector2[] sourceData =
			{
				new Vector2(0, -DoubleDiagonal),
				new Vector2(Diagonal, -Diagonal),
				new Vector2(DoubleDiagonal, 0),
				new Vector2(TripleDiagonal, Diagonal),
				new Vector2(DoubleDiagonal, DoubleDiagonal),
				new Vector2(Diagonal, TripleDiagonal),
				new Vector2(0, QuadrupleDiagonal),
				new Vector2(-Diagonal, TripleDiagonal),
				new Vector2(-DoubleDiagonal, DoubleDiagonal),
				new Vector2(-TripleDiagonal, Diagonal),
				new Vector2(-DoubleDiagonal, 0),
				new Vector2(-Diagonal, -Diagonal)
			};

			// These colors were computed in Paint.NET by cycling hue from 0 to 360 (in increments of 30) with maximum saturation and
			// brightness.
			Color[] colors =
			{
				Color.FromArgb(255, 0, 0),
				Color.FromArgb(255, 127, 0),
				Color.FromArgb(255, 255, 0),
				Color.FromArgb(127, 255, 0),
				Color.FromArgb(0, 255, 0),
				Color.FromArgb(0, 255, 127),
				Color.FromArgb(0, 255, 255),
				Color.FromArgb(0, 127, 255),
				Color.FromArgb(0, 0, 255),
				Color.FromArgb(127, 0, 255),
				Color.FromArgb(255, 0, 255),
				Color.FromArgb(255, 0, 127)
			};

			outerVertexData = new Vector2[ColorCount * 4];
			outerColorData = new Vector4[outerVertexData.Length];
			outerIndexData = Enumerable.Range(0, outerVertexData.Length).ToArray();

			for (int i = 0; i < ColorCount; i++)
			{
				int indexStart = i * 4;

				Vector2 source = center + sourceData[i];
				Vector4 color = colors[i].ToVector4();

				for (int j = 0; j < 4; j++)
				{
					outerVertexData[indexStart + j] = source + diamondData[j];
					outerColorData[indexStart + j] = color;
				}
			}

			innerVertexData = new[]
			{
				new Vector2(0, -DoubleDiagonal),
				new Vector2(DoubleDiagonal, 0),
				new Vector2(0, DoubleDiagonal),
				new Vector2(0, DoubleDiagonal),
				new Vector2(-DoubleDiagonal, 0),
				new Vector2(0, -DoubleDiagonal)
			};

			for (int i = 0; i < innerVertexData.Length; i++)
			{
				innerVertexData[i] += center;
			}

			Vector4 white = Vector4.One;
			Vector4 black = new Vector4(0, 0, 0, 1);

			innerColorData = new[]
			{
				white,
				white,
				white,
				black,
				black,
				black
			};

			innerIndexData = Enumerable.Range(0, 6).ToArray();
		}

		public override void Draw(SpriteBatch sb)
		{
			sb.DrawPrimitives(BeginMode.Triangles, innerVertexData, innerColorData, innerIndexData, 2);
			sb.DrawPrimitives(BeginMode.Quads, outerVertexData, outerColorData, outerIndexData, 12);
		}
	}
}
