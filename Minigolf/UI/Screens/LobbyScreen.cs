﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Minigolf.Input.Controls;
using Minigolf.Input.Data;
using Minigolf.Input.Sets;
using Minigolf.Input.Validation;
using Minigolf.Networking;

namespace Minigolf.UI.Screens
{
	public class LobbyScreen : Screen
	{
		public LobbyScreen() : base("LobbyScreen.json")
		{
		}
	}
}
