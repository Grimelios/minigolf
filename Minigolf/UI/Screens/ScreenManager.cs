﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Minigolf.Core;
using Minigolf.Graphics;
using Minigolf.Input.Data;
using Minigolf.Interfaces;

namespace Minigolf.UI.Screens
{
	public class ScreenManager : IDynamic, IRenderable2D
	{
		private Screen[] screens;
		private Screen activeScreen;

		public ScreenManager()
		{
			screens = new Screen[]
			{
				new LobbyScreen(),
				new CustomizationScreen()
			};

			activeScreen = screens[1];

			Messaging.Subscribe(MessageTypes.Input, (data, dt) => HandleInput((AggregateData)data));
		}

		private void HandleInput(AggregateData data)
		{
			activeScreen?.HandleInput(data);
		}

		public void Update(float dt)
		{
			activeScreen?.Update(dt);
		}

		public void Draw(SpriteBatch sb)
		{
			activeScreen?.Draw(sb);
		}
	}
}
