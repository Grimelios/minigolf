﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Minigolf.Core;
using Minigolf.Graphics;
using Minigolf.Input.Controls;
using Minigolf.Input.Data;
using Minigolf.Input.Sets;
using Minigolf.Interfaces;
using Minigolf.Json;
using Minigolf.UI.Screens.Panels;
using OpenTK;
using OpenTK.Input;

namespace Minigolf.UI.Screens
{
	public abstract class Screen : UIContainer
	{
		private SelectableSet<InputControl> controlSet;

		protected Screen(string jsonFile) : base(jsonFile)
		{
			controlSet = new SelectableSet<InputControl>();

			foreach (ScreenPanel panel in Elements.Cast<ScreenPanel>())
			{
				controlSet.Items.AddRange(panel.Controls);
			}
		}

		public virtual void HandleInput(AggregateData data)
		{
			controlSet.HandleMouse((MouseData)data[InputTypes.Mouse]);

			if (controlSet.FocusedItem != null)
			{
				Textbox textbox = controlSet.FocusedItem as Textbox;
				textbox?.HandleKeyboard((KeyboardData)data[InputTypes.Keyboard]);
			}
		}
	}
}
