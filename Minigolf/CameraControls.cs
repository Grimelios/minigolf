﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Minigolf.Input;

namespace Minigolf
{
	public class CameraControls
	{
		public List<InputBind> Forward { get; set; }
		public List<InputBind> Back { get; set; }
		public List<InputBind> Left { get; set; }
		public List<InputBind> Right { get; set; }
		public List<InputBind> Up { get; set; }
		public List<InputBind> Down { get; set; }
	}
}
