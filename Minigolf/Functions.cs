﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Minigolf.Core;
using Minigolf.Interfaces;
using OpenTK;

namespace Minigolf
{
	public static class Functions
	{
		public static int EnumCount<T>()
		{
			return Enum.GetValues(typeof(T)).Length;
		}

		public static T EnumParse<T>(string value)
		{
			return (T)Enum.Parse(typeof(T), value);
		}

		public static float ComputeAngle(Vector2 vector)
		{
			return ComputeAngle(Vector2.Zero, vector);
		}

		public static float ComputeAngle(Vector2 start, Vector2 end)
		{
			float dX = end.X - start.X;
			float dY = end.Y - start.Y;

			return (float)Math.Atan2(dX, dY);
		}

		public static Vector2 ComputeDirection(float angle)
		{
			float x = (float)Math.Cos(angle);
			float y = (float)Math.Sin(angle);

			return new Vector2(x, y);
		}

		public static Vector2 ComputeOrigin(Alignments2D alignment, int width, int height)
		{
			bool left = (alignment & Alignments2D.Left) > 0;
			bool right = (alignment & Alignments2D.Right) > 0;
			bool top = (alignment & Alignments2D.Top) > 0;
			bool bottom = (alignment & Alignments2D.Bottom) > 0;

			int x = left ? 0 : (right ? width : width / 2);
			int y = top ? 0 : (bottom ? height : height / 2);

			return new Vector2(x, y);
		}

		public static Color Lerp(Color color1, Color color2, float blend)
		{
			int Lerp(byte b1, byte b2) => (int)(b1 + (b2 - b1) * blend);

			int r = Lerp(color1.R, color2.R);
			int g = Lerp(color1.G, color2.G);
			int b = Lerp(color1.B, color2.B);
			int a = Lerp(color1.A, color2.A);

			return Color.FromArgb(a, r, g, b);
		}

		public static string RemoveExtension(string value)
		{
			return value.Substring(0, value.LastIndexOf('.'));
		}

		public static void PositionItems<T>(List<T> items, Vector2 basePosition, Vector2 spacing) where T : IPositionable2D
		{
			PositionItems(items.ToArray(), basePosition, spacing);
		}

		public static void PositionItems<T>(T[] items, Vector2 basePosition, Vector2 spacing) where T : IPositionable2D
		{
			for (int i = 0; i < items.Length; i++)
			{
				items[i].Position = basePosition + spacing * i;
			}
		}
	}
}
