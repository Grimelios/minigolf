﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;

namespace Minigolf.Shapes
{
	[DebuggerDisplay("{Width} x {Height} x {Depth}")]
	public class Box : Shape3D
	{
		public Box(float width, float height, float depth) :
			this(width, height, depth, Vector3.Zero)
		{
		}

		public Box(float width, float height, float depth, Vector3 position) : base(position)
		{
			Width = width;
			Height = height;
			Depth = depth;
		}

		public float Width { get; set; }
		public float Height { get; set; }
		public float Depth { get; set; }
	}
}
