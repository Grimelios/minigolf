﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Minigolf.Interfaces;
using OpenTK;

namespace Minigolf.Shapes
{
	public abstract class Shape3D : IPositionable3D
	{
		protected Shape3D(Vector3 position)
		{
			Position = position;
		}

		public Vector3 Position { get; set; }
	}
}
