﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Minigolf.Markdown
{
	[Flags]
	public enum MarkdownTypes
	{
		Regular = 0,
		Italic = 1,
		Bold = 2,
		BoldItalic = 3
	}

	public class MarkdownToken
	{
		public MarkdownToken(int start, MarkdownTypes type)
		{
			Start = start;
			Type = type;
		}

		public int Start { get; }
		public int End { get; set; }

		public MarkdownTypes Type { get; set; }
	}
}
