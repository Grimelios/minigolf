﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Minigolf.Markdown
{
	public static class MarkdownUtilities
	{
		private const string Italic = "*";
		private const string Bold = "**";
		private const string BoldItalic = "***";
		private const string Underline = "_";
		private const string Strikethrough = "~";

		private static string[] markdownArray =
		{
			Italic,
			Bold,
			BoldItalic,
			Underline,
			Strikethrough
		};

		public static void ComputeMarkdown(string value)
		{
			int index = 0;
			int regularIndex = 0;

			List<MarkdownToken> openTokens = new List<MarkdownToken>();

			do
			{
				for (int i = 0; i < openTokens.Count; i++)
				{
				}

				if (value.ContainsAt(BoldItalic, index))
				{
					openTokens.Add(new MarkdownToken(index + 3, MarkdownTypes.BoldItalic));
				}
			}
			while (index < value.Length);
		}
	}
}
