﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Minigolf
{
	using Receiver = Action<object, float>;

	public enum MessageTypes
	{
		// Input messages
		Keyboard,
		Mouse,
		Input,

		// Network messages
		Connect,
		Network,
		PlayerAccept,
		PlayerJoin,
		PlayerLeave,
		PlayerKick,
		PlayerName,
		PlayerReady,

		// Gameplay messages
		RoundStart,
		RoundEnd,
		HoleStart,
		HoleEnd,
		Chat,
		Stroke,
		BallReset,

		// Other messages
		Resize,
		AudioSettings,
		VideoSettings,
		GameplaySettings,
		Exit
	}

	public static class Messaging
	{
		private static List<ReceiverData> addList;
		private static List<ReceiverData> removeList;
		private static List<Receiver>[] receivers;

		static Messaging()
		{
			addList = new List<ReceiverData>();
			removeList = new List<ReceiverData>();
			receivers = new List<Receiver>[Functions.EnumCount<MessageTypes>()];
		}

		public static void Subscribe(MessageTypes messageType, Receiver action)
		{
			addList.Add(new ReceiverData(messageType, action));
		}

		public static void Unsubscribe(MessageTypes messageType, Receiver action)
		{
			removeList.Add(new ReceiverData(messageType, action));
		}

		public static void Send(MessageTypes messageType, object data, float dt = 0)
		{
			VerifyList(messageType).ForEach(a => a(data, dt));
		}

		public static void ProcessChanges()
		{
			removeList.ForEach(d => VerifyList(d.MessageType).Add(d.Action));
			removeList.Clear();

			addList.ForEach(d => VerifyList(d.MessageType).Add(d.Action));
			addList.Clear();
		}

		private static List<Receiver> VerifyList(MessageTypes messageType)
		{
			int index = (int)messageType;

			return receivers[index] ?? (receivers[index] = new List<Receiver>());
		}

		private class ReceiverData
		{
			public ReceiverData(MessageTypes messageType, Receiver action)
			{
				MessageType = messageType;
				Action = action;
			}

			public MessageTypes MessageType { get; }

			public Receiver Action { get; }
		}
	}
}
