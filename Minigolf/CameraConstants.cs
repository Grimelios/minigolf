﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Minigolf
{
	public static class CameraConstants
	{
		public const int FarPlane = 1000;
		public const int StartingDistance = 5;
		public const int MaxDistance = 12;
		public const int MaxSpeed = 30;
		public const int Acceleration = 100;
		public const int Deceleration = 100;

		public const float NearPlane = 0.1f;
		public const float MinDistance = 0.2f;
	}
}
