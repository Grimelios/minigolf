﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;

namespace Minigolf
{
	public static class Extensions
	{
		public static Point ToPoint(this Vector2 value)
		{
			return new Point((int)value.X, (int)value.Y);
		}

		public static Vector2 ToIntegers(this Vector2 value)
		{
			return new Vector2((int)value.X, (int)value.Y);
		}

		public static Vector2 ToVector2(this Point point)
		{
			return new Vector2(point.X, point.Y);
		}

		public static Vector4 ToVector4(this Color color)
		{
			return new Vector4(color.R, color.G, color.B, color.A) / 255;
		}

		public static Matrix4 ToMatrix4(this Matrix3 matrix)
		{
			// This extension function is used when converting 3x3 orientation matrices to 4x4 matrices.
			return new Matrix4(matrix)
			{
				M44 = 1
			};
		}

		public static bool ContainsAt(this string s, string value, int index)
		{
			if (value.Length == 1)
			{
				return s[index] == value[0];
			}

			if (index > s.Length - value.Length)
			{
				return false;
			}

			for (int i = 0; i < value.Length; i++)
			{
				if (s[index + i] != value[i])
				{
					return false;
				}
			}

			return true;
		}
	}
}
