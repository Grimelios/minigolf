﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Minigolf.Core;
using Minigolf.Interfaces;
using OpenTK;

namespace Minigolf
{
	public enum FadeTypes
	{
		FadeIn,
		FadeOut
	}

	public static class Smoothers
	{
		public static Timer CreateFade(IColorable target, int duration, FadeTypes fadeType, EaseTypes easeType)
		{
			Color start = fadeType == FadeTypes.FadeIn ? Color.Transparent : Color.White;
			Color end = fadeType == FadeTypes.FadeIn ? Color.White : Color.Transparent;

			return CreateColorTimer(target, start, end, duration, easeType);
		}

		public static Timer CreateColorTimer(IColorable target, Color start, Color end, int duration, EaseTypes ease)
		{
			return new Timer(duration,
				time => { target.Color = end; },
				progress => { target.Color = Functions.Lerp(start, end, Easing.Ease(progress, ease)); });
		}

		public static Timer CreatePositionTimer(IPositionable2D target, Vector2 start, Vector2 end, int duration, EaseTypes ease)
		{
			return new Timer(duration,
				time => { target.Position = end; },
				progress => { target.Position = Vector2.Lerp(start, end, Easing.Ease(progress, ease)); });
		}

		public static Timer CreateScale(IScalable2D target, Vector2 start, Vector2 end, int duration, EaseTypes easeType)
		{
			return new Timer(duration,
				time => target.Scale = end,
				progress => target.Scale = Vector2.Lerp(start, end, Easing.Ease(progress, easeType)));
		}
	}
}
