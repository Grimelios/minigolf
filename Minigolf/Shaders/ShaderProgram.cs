﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK.Graphics.OpenGL;

namespace Minigolf.Shaders
{
	public class ShaderProgram
	{
		private const int BufferSize = 256;

		private int[] attributes;
		private int[] uniforms;

		public ShaderProgram(string name)
		{
			ProgramId = GL.CreateProgram();

			Attributes = new Dictionary<string, int>();
			Uniforms = new Dictionary<string, int>();
			Buffers = new Dictionary<string, uint>();
			
			Load(name + ".vert", ShaderType.VertexShader);
			Load(name + ".frag", ShaderType.FragmentShader);
			Link();
			GenerateBuffers();
		}

		public int ProgramId { get; }

		public Dictionary<string, int> Attributes { get; }
		public Dictionary<string, int> Uniforms { get; }
		public Dictionary<string, uint> Buffers { get; }

		private void Load(string filename, ShaderType shaderType)
		{
			int address = GL.CreateShader(shaderType);

			using (StreamReader reader = new StreamReader(Paths.Shaders + filename))
			{
				GL.ShaderSource(address, reader.ReadToEnd());
			}

			GL.CompileShader(address);
			GL.AttachShader(ProgramId, address);
		}

		private void Link()
		{
			GL.LinkProgram(ProgramId);
			GL.GetProgram(ProgramId, GetProgramParameterName.ActiveAttributes, out int attributeCount);
			GL.GetProgram(ProgramId, GetProgramParameterName.ActiveUniforms, out int uniformCount);

			attributes = new int[attributeCount];
			uniforms = new int[uniformCount];

			StringBuilder nameBuilder = new StringBuilder();

			for (int i = 0; i < attributeCount; i++)
			{
				nameBuilder.Clear();

				GL.GetActiveAttrib(ProgramId, i, BufferSize, out int length, out int size, out ActiveAttribType type, nameBuilder);

				string name = nameBuilder.ToString();

				int address = GL.GetAttribLocation(ProgramId, nameBuilder.ToString());

				attributes[i] = address;
				Attributes.Add(name, address);
			}

			for (int i = 0; i < uniformCount; i++)
			{
				nameBuilder.Clear();

				GL.GetActiveUniform(ProgramId, i, BufferSize, out int length, out int size, out ActiveUniformType type, nameBuilder);

				string name = nameBuilder.ToString();
		
				int address = GL.GetUniformLocation(ProgramId, name);

				uniforms[i] = address;
				Uniforms.Add(name, address);
			}
		}

		private void GenerateBuffers()
		{
			List<string> names = new List<string>();
			names.AddRange(Attributes.Keys);
			names.AddRange(Uniforms.Keys);

			foreach (string name in names)
			{
				GL.GenBuffers(1, out uint buffer);

				Buffers.Add(name, buffer);
			}
		}

		public void Use()
		{
			GL.UseProgram(ProgramId);
		}

		public void EnableVertexAttributeArrays()
		{
			foreach (int address in attributes)
			{
				GL.EnableVertexAttribArray(address);
			}
		}

		public void DisableVertexAttributeArrays()
		{
			foreach (int address in attributes)
			{
				GL.DisableVertexAttribArray(address);
			}
		}
	}
}
