﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Jitter;
using Jitter.Dynamics;
using Jitter.LinearMath;
using OpenTK;

namespace Minigolf.Physics
{
	public static class PhysicsUtilities
	{
		private static World world;

		public static void Initialize(World w)
		{
			world = w;
		}

		public static void Raycast(Vector3 start, Vector3 direction, float range)
		{
			world.CollisionSystem.Raycast(start, direction, RaycastFunction, out RigidBody body, out JVector normal, out float fraction);

			Vector3 position = start + direction * fraction;
		}

		private static bool RaycastFunction(RigidBody body, JVector normal, float fraction)
		{
			return true;
		}
	}
}
