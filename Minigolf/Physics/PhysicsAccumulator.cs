﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Jitter;
using Minigolf.Interfaces;

namespace Minigolf.Physics
{
	public class PhysicsAccumulator : IDynamic
	{
		private const float PhysicsStep = 1f / 120;
		private const int MaxSteps = 8;

		private World world;

		public PhysicsAccumulator(World world)
		{
			this.world = world;
		}

		public void Update(float dt)
		{
			world.Step(dt, true, PhysicsStep, MaxSteps);
		}
	}
}
