﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Jitter;
using Jitter.Collision.Shapes;
using Jitter.Dynamics;
using Minigolf.Entities;
using Minigolf.Shapes;
using OpenTK;

namespace Minigolf.Physics
{
	public enum BodyTypes
	{
		Dynamic,
		Static
	}

	public static class PhysicsFactory
	{
		private static World world;

		public static void Initialize(World w)
		{
			world = w;
		}

		public static RigidBody CreateBox(Box box, BodyTypes bodyType, Entity entity)
		{
			return CreateBox(box.Width, box.Height, box.Depth, box.Position, bodyType, entity);
		}

		public static RigidBody CreateBox(float width, float height, float depth, Vector3 position, BodyTypes bodyType, Entity entity)
		{
			RigidBody body = new RigidBody(new BoxShape(width, height, depth))
			{
				Position = position,
				IsStatic = bodyType == BodyTypes.Static,
				Tag = entity
			};

			world.AddBody(body);

			return body;
		}

		public static RigidBody CreateSphere(float radius, BodyTypes bodyType, Entity entity)
		{
			RigidBody body = new RigidBody(new SphereShape(radius))
			{
				IsStatic = bodyType == BodyTypes.Static,
				Tag = entity
			};

			world.AddBody(body);

			return body;
		}
	}
}
