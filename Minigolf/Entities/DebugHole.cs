﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Jitter.Dynamics;
using Minigolf.Content;
using Minigolf.Physics;
using Minigolf.Shapes;
using OpenTK;

namespace Minigolf.Entities
{
	public class DebugHole : Entity
	{
		private RigidBody[] bodies;

		public DebugHole()
		{
			Model = ContentLoader.LoadModel("DebugHole.obj");

			Boxes = new[]
			{
				new Box(2, 0.2f, 14, new Vector3(0, -0.2f, 0)),
				new Box(0.1f, 0.2f, 14, new Vector3(-0.95f, 0, 0)),
				new Box(0.1f, 0.2f, 14, new Vector3(0.95f, 0, 0)),
				new Box(2, 0.2f, 0.1f, new Vector3(0, 0, -6.95f)),
				new Box(2, 0.2f, 0.1f, new Vector3(0, 0, 6.95f)),
			};

			bodies = Boxes.Select(b => PhysicsFactory.CreateBox(b, BodyTypes.Static, this)).ToArray();
		}

		public override Vector3 Position
		{
			get => base.Position;
			set
			{
				for (int i = 0; i < bodies.Length; i++)
				{
					bodies[i].Position = value + Boxes[i].Position;
				}

				base.Position = value;
			}
		}

		public Box[] Boxes { get; }
	}
}
