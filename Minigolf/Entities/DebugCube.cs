﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Minigolf.Content;
using Minigolf.Graphics;
using Minigolf.Physics;
using OpenTK;

namespace Minigolf.Entities
{
	public class DebugCube : Entity
	{
		public DebugCube()
		{
			//Body = PhysicsFactory.CreateBox(2, 2, 2, Vector3.Zero, BodyTypes.Dynamic, this);
			Model = ContentLoader.LoadModel("Cube.obj");
		}
	}
}
