﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Jitter.Dynamics;
using Minigolf.Graphics;
using Minigolf.Interfaces;
using OpenTK;

namespace Minigolf.Entities
{
	public abstract class Entity : IPositionable3D, IOrientable, IScalable3D, IColorable, IDynamic
	{
		private Vector3 position;
		private Matrix3 orientation;

		private bool selfUpdate;

		protected Entity()
		{
			Orientation = Matrix3.Identity;
			Scale = Vector3.One;
			Color = Color.White;
		}

		protected RigidBody Body { get; set; }

		public virtual Vector3 Position
		{
			get => position;
			set
			{
				position = value;

				if (Body != null && !selfUpdate)
				{
					Body.Position = value;
				}
			}
		}

		public Matrix3 Orientation
		{
			get => orientation;
			set
			{
				orientation = value;

				if (Body != null && !selfUpdate)
				{
					Body.Orientation = value;
				}
			}
		}

		public Vector3 Scale { get; set; }
		public Color Color { get; set; }
		public Model Model { get; protected set; }

		// The model matrix is stored for each entity so that it (the matrix) doesn't need to be recomputed each frame if the entity
		// doesn't change.
		public Matrix4 ModelMatrix { get; private set; }

		public virtual void Update(float dt)
		{
			if (Body != null)
			{
				selfUpdate = true;
				Position = Body.Position;
				Orientation = Body.Orientation;
				selfUpdate = false;
			}

			ModelMatrix = Matrix4.CreateScale(Scale) * Orientation.ToMatrix4() * Matrix4.CreateTranslation(Position);
		}
	}
}
