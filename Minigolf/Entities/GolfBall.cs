﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Minigolf.Content;
using Minigolf.Physics;

namespace Minigolf.Entities
{
	public class GolfBall : Entity
	{
		private const float Radius = 0.1f;

		public GolfBall()
		{
			Body = PhysicsFactory.CreateSphere(Radius, BodyTypes.Dynamic, this);
			Model = ContentLoader.LoadModel("GolfBall.obj");
		}
	}
}
