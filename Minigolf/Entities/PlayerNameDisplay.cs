﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Minigolf.Core;

namespace Minigolf.Entities
{
	public class PlayerNameDisplay : Entity
	{
		private SpriteText spriteText;

		public PlayerNameDisplay(string name, Color color)
		{
		}

		// The camera reference is required to billboard the player name towards the camera.
		public Camera3D Camera { get; set; }
	}
}
