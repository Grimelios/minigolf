﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Minigolf.Networking
{
	public class ConnectionData
	{
		public ConnectionData(IPAddress address, int port)
		{
			Address = address;
			Port = port;
		}

		public IPAddress Address { get; }

		public int Port { get; }
	}
}
