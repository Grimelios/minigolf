﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lidgren.Network;

namespace Minigolf.Networking.Encoders
{
	public abstract class NetworkEncoder
	{
		public virtual void Encode(NetworkData data, NetOutgoingMessage message)
		{
			message.Write((byte)data.DataType);
		}

		public abstract object Decode(NetIncomingMessage message);
	}
}
