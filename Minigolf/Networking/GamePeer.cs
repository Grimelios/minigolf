﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Lidgren.Network;
using Minigolf.Networking.Encoders;

namespace Minigolf.Networking
{
	public class GamePeer
	{
		private NetPeer peer;
		private NetworkEncoder[] encoders;

		public GamePeer()
		{
			NetPeerConfiguration configuration = new NetPeerConfiguration("Minigolf");

			peer = new NetPeer(configuration);
			peer.Start();

			encoders = new NetworkEncoder[]
			{
			};

			Messaging.Subscribe(MessageTypes.Connect, (data, dt) => Connect((ConnectionData)data));
			Messaging.Subscribe(MessageTypes.Network, (data, dt) => Send((NetworkData)data));
		}

		private void Connect(ConnectionData data)
		{
			peer.Connect(new IPEndPoint(data.Address, data.Port));
		}

		private void Send(NetworkData data)
		{
		}

		public void Update()
		{
			NetIncomingMessage message;

			while ((message = peer.ReadMessage()) != null)
			{
				switch (message.MessageType)
				{
					case NetIncomingMessageType.Data:
						break;
				}

				peer.Recycle(message);
			}
		}
	}
}
