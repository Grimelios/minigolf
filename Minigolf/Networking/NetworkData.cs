﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Minigolf.Networking
{
	public enum NetworkDataTypes
	{
		PlayerJoin,
		PlayerLeave,
		PlayerKick,
		PlayerName,
		BallStroke,
		BallPosition,
		BallReset,
		Chat
	}

	public class NetworkData
	{
		public NetworkData(NetworkDataTypes dataType, object data)
		{
			DataType = dataType;
			Data = data;
		}

		public NetworkDataTypes DataType { get; }

		public object Data { get; }
	}
}
