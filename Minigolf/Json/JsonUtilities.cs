﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Minigolf.Json
{
	public static class JsonUtilities
	{
		public static T Deserialize<T>(string filename, bool useTypeNameHandling)
		{
			JsonSerializerSettings settings = new JsonSerializerSettings();

			if (useTypeNameHandling)
			{
				settings.TypeNameHandling = TypeNameHandling.Auto;
			}

			return JsonConvert.DeserializeObject<T>(File.ReadAllText(Paths.Json + filename), settings);
		}

		public static void Serialize(object data, string filename)
		{
			using (StreamWriter writer = new StreamWriter(Paths.Json + filename))
			{
				writer.Write(JsonConvert.SerializeObject(data));
			}
		}
	}
}
