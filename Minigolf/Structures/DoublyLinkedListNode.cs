﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Minigolf.Structures
{
	public class DoublyLinkedListNode<T>
	{
		public DoublyLinkedListNode(T data)
		{
			Data = data;
		}

		public T Data { get; }

		public DoublyLinkedListNode<T> Next { get; set; }
		public DoublyLinkedListNode<T> Previous { get; set; }
	}
}
