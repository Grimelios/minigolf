﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Minigolf.Structures
{
	public class DoublyLinkedList<T>
	{
		public DoublyLinkedListNode<T> Head { get; private set; }
		public DoublyLinkedListNode<T> Tail { get; private set; }

		public int Count { get; private set; }

		public DoublyLinkedList()
		{
		}

		public DoublyLinkedList(IEnumerable<T> items)
		{
			foreach (T item in items)
			{
				Add(item);
			}
		}

		public void Clear()
		{
			Head = null;
			Tail = null;
			Count = 0;
		}

		public void Add(T data)
		{
			Add(new DoublyLinkedListNode<T>(data));
		}

		public void Add(DoublyLinkedListNode<T> node)
		{
			if (Count == 0)
			{
				Head = node;
				Tail = node;
			}
			else
			{
				node.Previous = Tail;
				Tail.Next = node;
				Tail = node;
			}

			Count++;
		}

		public void Remove(T data)
		{
			DoublyLinkedListNode<T> node = Head;

			do
			{
				if (node.Data.Equals(data))
				{
					Remove(node);

					return;
				}

				node = node.Next;
			}
			while (node != null);
		}

		public void Remove(DoublyLinkedListNode<T> node)
		{
			if (Count == 1)
			{
				Head = null;
				Tail = null;
			}
			else if (node == Head)
			{
				Head = node.Next;
				Head.Previous = null;
			}
			else if (node == Tail)
			{
				Tail = node.Previous;
				Tail.Next = null;
			}
			else
			{
				node.Previous.Next = node.Next;
				node.Next.Previous = node.Previous;
			}

			Count--;
		}
	}
}
