﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Minigolf.Core;
using Minigolf.Graphics;
using Minigolf.Interfaces;
using Minigolf.Networking;
using Minigolf.UI.Menus;
using Minigolf.UI.Screens;

namespace Minigolf.Loops
{
	public abstract class GameLoop : IDynamic
	{
		public GamePeer Peer { get; set; }
		public Camera3D Camera { get; set; }
		public SpriteBatch SpriteBatch { get; set; }
		public MenuManager MenuManager { get; set; }
		public ScreenManager ScreenManager { get; set; }
		public PrimitiveBatch PrimitiveBatch { get; set; }

		public abstract void Initialize();
		public abstract void Update(float dt);
		public abstract void Draw();
	}
}
