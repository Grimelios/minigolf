﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Jitter;
using Jitter.Collision;
using Jitter.Dynamics;
using Jitter.LinearMath;
using Minigolf.Content;
using Minigolf.Control;
using Minigolf.Core;
using Minigolf.Data;
using Minigolf.Entities;
using Minigolf.Graphics;
using Minigolf.Input.Data;
using Minigolf.Physics;
using Minigolf.Settings;
using Minigolf.Shapes;
using Minigolf.UI.Hud;
using Minigolf.UI.Screens;
using OpenTK;
using OpenTK.Input;

namespace Minigolf.Loops
{
	public class GameplayLoop : GameLoop
	{
		private HeadsUpDisplay headsUpDisplay;
		private ModelRenderer modelRenderer;
		private DebugHole debugHole;
		private GolfBall golfBall;
		private ShadowTester shadowTester;
		private CameraController cameraController;
		private StrokeController strokeController;
		private PhysicsAccumulator accumulator;

		public override void Initialize()
		{
			headsUpDisplay = new HeadsUpDisplay();
			modelRenderer = new ModelRenderer(Camera);
			shadowTester = new ShadowTester(Camera);

			CollisionSystem collisionSystem = new CollisionSystemSAP();
			collisionSystem.CollisionDetected += (body1, body2, point1, point2, normal, pDepth) =>
			{
				body1.SignalCollision(body2, point1, point2, normal, pDepth);
				body2.SignalCollision(body1, point2, point1, normal, pDepth);
			};

			World world = new World(collisionSystem);
			world.Gravity = new JVector(0, -PhysicsConstants.Gravity, 0);
			world.Events.BodiesEndCollide += (body1, body2) =>
			{
				body1.SignalSeparation(body2);
				body2.SignalSeparation(body1);
			};

			accumulator = new PhysicsAccumulator(world);
			cameraController = new CameraController(Camera);
			strokeController = new StrokeController(Camera);

			PhysicsFactory.Initialize(world);
			PhysicsUtilities.Initialize(world);

			golfBall = new GolfBall();
			debugHole = new DebugHole();

			HoleData holeData = new HoleData
			{
				Name = "Photosynthesis",
				Number = 4,
				Par = 3
			};

			CourseData courseData = new CourseData
			{
				Name = "Volcano",
				Holes = new[] { holeData }
			};

			RoundSettings roundSettings = new RoundSettings
			{
				Course = courseData.Name,
				CourseVariation = 0,
				MinuteLimit = 2,
				SecondLimit = 30,
				GravityScale = 1,
				GameMode = GameModes.Traditional
			};

			VideoSettings videoSettings = new VideoSettings
			{
				FieldOfView = MathHelper.PiOver2,
				WindowWidth = 800,
				WindowHeight = 600,
				WindowMode = WindowModes.Windowed
			};

			GameplaySettings gameplaySettings = new GameplaySettings
			{
				InvertX = false,
				InvertY = false,
				MiddleDrag = true,
				MouseSensitivity = 0.1f,
				ScrollSensitivity = 0.25f
			};

			Messaging.ProcessChanges();
			Messaging.Send(MessageTypes.VideoSettings, videoSettings);
			Messaging.Send(MessageTypes.GameplaySettings, gameplaySettings);
			Messaging.Send(MessageTypes.RoundStart, roundSettings);

			Messaging.Subscribe(MessageTypes.Keyboard, (data, dt) =>
			{
				if (((KeyboardData)data).Query(Key.H, InputStates.PressedThisFrame))
				{
					Messaging.Send(MessageTypes.HoleStart, holeData);
				}
			});
		}
		
		public override void Update(float dt)
		{
			accumulator.Update(dt);
			shadowTester.Update(dt);
			headsUpDisplay.Update(dt);
		}

		public override void Draw()
		{
			SpriteBatch sb = SpriteBatch;
			PrimitiveBatch pb = PrimitiveBatch;
			
			sb.Begin();
			pb.Begin();
			
			shadowTester.Draw();
			headsUpDisplay.Draw(sb);

			sb.End();
			pb.End();
		}
	}
}
