﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Minigolf.Graphics;

namespace Minigolf.Loops
{
	public class TitleLoop : GameLoop
	{
		public override void Initialize()
		{
		}

		public override void Update(float dt)
		{
			MenuManager.Update(dt);
		}

		public override void Draw()
		{
			SpriteBatch sb = SpriteBatch;

			sb.Begin();
			MenuManager.Draw(sb);
			sb.End();
		}
	}
}
