﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Minigolf.Core;
using Minigolf.Graphics;
using Minigolf.UI.Screens;

namespace Minigolf.Loops
{
	public class LobbyLoop : GameLoop
	{
		public override void Initialize()
		{
		}

		public override void Update(float dt)
		{
			ScreenManager.Update(dt);
		}

		public override void Draw()
		{
			SpriteBatch sb = SpriteBatch;
			
			sb.Begin();
			ScreenManager.Draw(sb);
			sb.End();
		}
	}
}
