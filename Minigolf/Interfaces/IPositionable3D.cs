﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;

namespace Minigolf.Interfaces
{
	public interface IPositionable3D
	{
		Vector3 Position { get; set; }
	}
}
