﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;

namespace Minigolf.Interfaces
{
	public interface IScalable2D
	{
		Vector2 Scale { get; set; }
	}
}
