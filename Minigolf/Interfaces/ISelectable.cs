﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Minigolf.Interfaces
{
	public interface ISelectable : IBoundable2D
	{
		bool Enabled { get; set; }

		void OnHover();
		void OnUnhover();
		void OnHighlight();
		void OnUnhighlight();
		bool OnSelect();
	}
}
