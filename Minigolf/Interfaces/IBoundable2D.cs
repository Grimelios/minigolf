﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Minigolf.Core;

namespace Minigolf.Interfaces
{
	public interface IBoundable2D
	{
		Bounds Bounds { get; }
	}
}
