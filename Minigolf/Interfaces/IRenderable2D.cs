﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Minigolf.Core;
using Minigolf.Graphics;

namespace Minigolf.Interfaces
{
	public interface IRenderable2D
	{
		void Draw(SpriteBatch sb);
	}
}
