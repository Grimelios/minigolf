﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;

namespace Minigolf.Interfaces
{
	public interface IOrientable
	{
		Matrix3 Orientation { get; set; }
	}
}
