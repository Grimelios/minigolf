﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Minigolf
{
	public class Program
	{
		private const int Framerate = 60;

		public static void Main(string[] args)
		{
			using (Game game = new Game())
			{
				game.Run(Framerate);
			}
		}
	}
}
