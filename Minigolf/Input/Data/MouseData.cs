﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;

namespace Minigolf.Input.Data
{
	public enum MouseButtons
	{
		Left,
		Right,
		Middle,
		Side1,
		Side2
	}

	public class MouseData : InputData
	{
		public MouseData(InputStates leftClick, InputStates rightClick, InputStates middleClick, InputStates side1Click, InputStates side2Click,
			Point position, Point previousPosition, float scrollDelta)
		{
			LeftClick = leftClick;
			RightClick = rightClick;
			MiddleClick = middleClick;
			Side1Click = side1Click;
			Side2Click = side2Click;
			Position = position;
			PreviousPosition = previousPosition;
			ScrollDelta = scrollDelta;
		}

		public InputStates LeftClick { get; }
		public InputStates RightClick { get; }
		public InputStates MiddleClick { get; }
		public InputStates Side1Click { get; }
		public InputStates Side2Click { get; }

		public Point Position { get; }
		public Point PreviousPosition { get; }
		public Point PositionDelta => new Point(Position.X - PreviousPosition.X, Position.Y - PreviousPosition.Y);

		public float ScrollDelta { get; }

		public bool Moved => Position != PreviousPosition;

		public override bool Query(object data, InputStates state)
		{
			switch ((MouseButtons)data)
			{
				case MouseButtons.Left: return (LeftClick & state) == state;
				case MouseButtons.Right: return (RightClick & state) == state;
				case MouseButtons.Middle: return (MiddleClick & state) == state;
				case MouseButtons.Side1: return (Side1Click & state) == state;
				case MouseButtons.Side2: return (Side2Click & state) == state;
			}

			return false;
		}
	}
}
