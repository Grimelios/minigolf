﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK.Input;

namespace Minigolf.Input.Data
{
	public class KeyboardData : InputData
	{
		private Dictionary<Key, InputStates> keyStates;

		public KeyboardData(Dictionary<Key, InputStates> keyStates, List<Key> keysDown, List<Key> keysPressedThisFrame,
			List<Key> keysReleasedThisFrame)
		{
			this.keyStates = keyStates;

			KeysDown = keysDown;
			KeysPressedThisFrame = keysPressedThisFrame;
			KeysReleasedThisFrame = keysReleasedThisFrame;
		}

		public List<Key> KeysDown { get; }
		public List<Key> KeysPressedThisFrame { get; }
		public List<Key> KeysReleasedThisFrame { get; }

		public bool Shift { get; set; }
		public bool Control { get; set; }
		public bool Alt { get; set; }

		public bool Query(Key key, InputStates state)
		{
			return (keyStates[key] & state) == state;
		}

		public override bool Query(object data, InputStates state)
		{
			return Query((Key)data, state);
		}

		public bool Query(InputStates state, params Key[] keys)
		{
			return keys.Any(k => Query(k, state));
		}
	}
}
