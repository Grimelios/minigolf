﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Minigolf.Input.Data
{
	[Flags]
	public enum InputStates
	{
		// Assigning values in this way allows held and released checks to also account for pressed and released this frame (since if a key
		// was pressed this frame, it still counts as held).
		Held = 1,
		Released = 4,
		PressedThisFrame = 3,
		ReleasedThisFrame = 12
	}

	public enum InputTypes
	{
		Keyboard,
		Mouse
	}

	public abstract class InputData
	{
		public abstract bool Query(object data, InputStates state);

		public bool Query(object data, params InputStates[] states)
		{
			return states.Any(s => Query(data, s));
		}
	}
}
