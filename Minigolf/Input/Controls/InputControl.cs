﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Minigolf.Core;
using Minigolf.Graphics;
using Minigolf.Input.Controls.Rendering;
using Minigolf.Interfaces;
using OpenTK;

namespace Minigolf.Input.Controls
{
	public abstract class InputControl : Container2D, ISelectable
	{
		protected InputControl(int width, int height)
		{
			Bounds = new Bounds(width, height);
		}

		protected bool Focusable { get; set; }

		protected InputRenderer Renderer { get; set; }

		public override Vector2 Position
		{
			get => base.Position;
			set
			{
				base.Position = value;

				Renderer.Position = value;
			}
		}

		public bool Enabled { get; set; } = true;

		public void OnHover()
		{
		}

		public void OnUnhover()
		{
		}

		public void OnHighlight()
		{
		}

		public void OnUnhighlight()
		{
		}

		public virtual bool OnSelect()
		{
			return Focusable;
		}

		public override void Draw(SpriteBatch sb)
		{
			Renderer.Draw(sb);
		}
	}
}
