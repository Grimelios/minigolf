﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Minigolf.Core;
using Minigolf.Graphics;
using OpenTK;

namespace Minigolf.Input.Controls.Rendering
{
	public class ButtonRenderer : InputRenderer
	{
		private Button parent;
		private SpriteText spriteText;

		public ButtonRenderer(Button parent)
		{
			this.parent = parent;

			spriteText = new SpriteText("Default", parent.Text, Alignments2D.Center);
		}

		public override Vector2 Position
		{
			get => spriteText.Position;
			set => spriteText.Position = value;
		}

		public string Text
		{
			set => spriteText.Value = value;
		}

		public override void Draw(SpriteBatch sb)
		{
			spriteText.Draw(sb);
			sb.DrawBounds(parent.Bounds, Color.White);
		}
	}
}
