﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Minigolf.Core;
using Minigolf.Graphics;
using OpenTK;

namespace Minigolf.Input.Controls.Rendering
{
	public class TextboxRenderer : InputRenderer
	{
		private Textbox parent;
		private SpriteText spriteText;

		public TextboxRenderer(Textbox parent)
		{
			this.parent = parent;

			spriteText = new SpriteText("Default", null);
		}

		public override Vector2 Position
		{
			get => base.Position;
			set
			{
				spriteText.Position = value;

				base.Position = value;
			}
		}

		public string Value
		{
			set => spriteText.Value = value;
		}

		public override void Draw(SpriteBatch sb)
		{
			spriteText.Draw(sb);
			sb.DrawBounds(parent.Bounds, Color.White);
		}
	}
}
