﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Minigolf.Core;
using Minigolf.Graphics;
using Minigolf.Interfaces;
using OpenTK;

namespace Minigolf.Input.Controls.Rendering
{
	public abstract class InputRenderer : IPositionable2D, IRenderable2D
	{
		private Vector2 position;

		public virtual Vector2 Position
		{
			get => position;
			set => position = value;
		}

		public abstract void Draw(SpriteBatch sb);
	}
}
