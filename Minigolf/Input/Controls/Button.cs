﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Minigolf.Input.Controls.Rendering;
using OpenTK;

namespace Minigolf.Input.Controls
{
	public class Button : InputControl
	{
		private Action submitAction;
		private ButtonRenderer renderer;

		private string text;

		public Button(string text, int width, int height, Action submitAction) : base(width, height)
		{
			this.text = text;
			this.submitAction = submitAction;

			renderer = new ButtonRenderer(this);
			Renderer = renderer;
			Centered = true;
		}

		public string Text
		{
			get => text;
			set
			{
				text = value;
				renderer.Text = value;
			}
		}

		public override bool OnSelect()
		{
			submitAction();

			return false;
		}
	}
}
