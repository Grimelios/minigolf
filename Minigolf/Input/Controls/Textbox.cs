﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Minigolf.Input.Controls.Rendering;
using Minigolf.Input.Data;
using OpenTK.Input;

namespace Minigolf.Input.Controls
{
	using TextboxValidator = Func<string, List<object>, bool>;
	using TextboxSubmitFunction = Action<List<object>>;

	public class Textbox : InputControl
	{
		private static char[] numericSpecials =
		{
			')',
			'!',
			'@',
			'#',
			'$',
			'%',
			'^',
			'&',
			'*',
			'('
		};

		private StringBuilder builder;
		private Action submitFunction;
		private TextboxRenderer renderer;

		private List<object> results;

		public Textbox(int width, int height, Action submitFunction, string label = null) : base(width, height)
		{
			this.submitFunction = submitFunction;

			Label = label;
			builder = new StringBuilder();
			renderer = new TextboxRenderer(this);
			Renderer = renderer;
			Focusable = true;
			Validators = new List<TextboxValidator>();
			results = new List<object>();
		}

		public string Label { get; set; }
		public string Value
		{
			get => builder.ToString();
			set
			{
				builder.Clear();
				builder.Append(value);

				renderer.Value = value;
			}
		}

		public int Cursor { get; private set; }

		public bool Insert { get; private set; }

		public List<TextboxValidator> Validators { get; }

		public bool Submit(out object result)
		{
			if (Validators.All(v => v(Value, results)))
			{
				result = results.Count > 0 ? results[0] : null;

				return true;
			}

			result = null;

			return false;
		}

		public void HandleKeyboard(KeyboardData data)
		{
			if (data.Query(InputStates.PressedThisFrame, Key.Enter, Key.KeypadEnter))
			{
				submitFunction();

				return;
			}
			
			bool capsLock = InputUtilities.CheckLock(LockKeys.CapsLock);
			bool numLock = InputUtilities.CheckLock(LockKeys.NumLock);

			// Even if numlock is off, special numpad functions can be activated by holding shift.
			if (!numLock)
			{
				numLock = data.Shift;
			}

			bool home = data.Query(Key.Home, InputStates.PressedThisFrame);
			bool end = data.Query(Key.End, InputStates.PressedThisFrame);
			bool left = data.Query(Key.Left, InputStates.PressedThisFrame);
			bool right = data.Query(Key.Right, InputStates.PressedThisFrame);
			bool backspace = data.Query(Key.Back, InputStates.PressedThisFrame);
			bool delete = data.Query(Key.Delete, InputStates.PressedThisFrame);

			if (!numLock)
			{
				home |= data.Query(Key.Keypad7, InputStates.PressedThisFrame);
				end |= data.Query(Key.Keypad1, InputStates.PressedThisFrame);
				left |= data.Query(Key.Keypad4, InputStates.PressedThisFrame);
				right |= data.Query(Key.Keypad6, InputStates.PressedThisFrame);
				delete |= data.Query(Key.KeypadDecimal, InputStates.PressedThisFrame);

				if (data.Query(Key.Keypad0, InputStates.PressedThisFrame))
				{
					Insert = !Insert;
				}
			}

			// If home and end are pressed on the same frame, home takes priority.
			if (home)
			{
				Cursor = 0;
			}
			else if (end)
			{
				Cursor = builder.Length;
			}

			if (left ^ right)
			{
				if (left)
				{
					Cursor = Cursor > 0 ? --Cursor : 0;
				}
				else
				{
					Cursor = Cursor < builder.Length ? ++Cursor : builder.Length;
				}
			}

			// Backspace and delete are intentionally done after moving the cursor and before adding new characters.
			if (backspace && builder.Length > 0 && Cursor > 0)
			{
				builder.Remove(Cursor - 1, 1);
				Cursor--;

				return;
			}

			if (delete && builder.Length > 0 && Cursor < builder.Length)
			{
				builder.Remove(Cursor, 1);
			}

			if (data.Query(Key.Insert, InputStates.PressedThisFrame))
			{
				Insert = !Insert;
			}

			foreach (Key key in data.KeysPressedThisFrame)
			{
				char? character = GetCharacter(key, data.Shift, capsLock, numLock);

				if (character.HasValue)
				{
					if (Insert && Cursor < builder.Length)
					{
						builder[Cursor] = character.Value;
					}
					else
					{
						builder.Insert(Cursor, character.Value);
					}

					Cursor++;
				}
			}

			renderer.Value = Value;
		}

		private char? GetCharacter(Key key, bool shift, bool capsLock, bool numLock)
		{
			string value = key.ToString();

			if (key >= Key.A && key <= Key.Z)
			{
				if (capsLock)
				{
					shift = !shift;
				}

				return shift ? value[0] : value.ToLower()[0];
			}

			if (key >= Key.Number0 && key <= Key.Number9)
			{
				return !shift ? value[6] : numericSpecials[value[6] - '0'];
			}

			// Special numpad functions are handled before reaching this point.
			if (numLock)
			{
				if (key >= Key.Keypad0 && key <= Key.Keypad9)
				{
					return value[6];
				}

				if (key == Key.KeypadDecimal)
				{
					return '.';
				}
			}

			switch (key)
			{
				case Key.Comma: return shift ? '<' : ',';
				case Key.Period: return shift ? '>' : '.';
				case Key.Slash: return shift ? '?' : '/';
				case Key.Semicolon: return shift ? ':' : ';';
				case Key.Quote: return shift ? '"' : '\'';
				case Key.BracketLeft: return shift ? '{' : '[';
				case Key.BracketRight: return shift ? '}' : ']';
				case Key.BackSlash: return shift ? '|' : '\\';
				case Key.Minus: return shift ? '_' : '-';
				case Key.Plus: return shift ? '+' : '=';
				case Key.Tilde: return shift ? '~' : '`';

				// These four keys work even if numlock is turned off.
				case Key.KeypadAdd: return '+';
				case Key.KeypadSubtract: return '-';
				case Key.KeypadMultiply: return '*';
				case Key.KeypadDivide: return '/';

				case Key.Space: return ' ';
			}

			return null;
		}
	}
}
