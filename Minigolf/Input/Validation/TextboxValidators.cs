﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Minigolf.Input.Validation
{
	using TextboxValidator = Func<string, List<object>, bool>;

	public class TextboxValidators
	{
		public static TextboxValidator Numeric => (v, results) =>
		{
			if (int.TryParse(v, out int result))
			{
				results.Add(result);

				return true;
			}

			return false;
		};

		public static TextboxValidator IPAddress =>	(v, results) =>
		{
			if (System.Net.IPAddress.TryParse(v, out IPAddress address))
			{
				results.Add(address);

				return true;
			}

			return false;
		};
		

		public static TextboxValidator Length(int targetLength)
		{
			return (v, results) => v.Length == targetLength;
		}
	}
}
