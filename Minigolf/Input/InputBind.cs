﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Minigolf.Input.Data;
using Newtonsoft.Json;
using OpenTK.Input;

namespace Minigolf.Input
{
	public class InputBind
	{
		[JsonConstructor]
		public InputBind(InputTypes inputType, string data)
		{
			InputType = inputType;
			Data = ParseData(inputType, data);
		}

		public InputBind(InputTypes inputType, object data)
		{
			InputType = inputType;
			Data = data;
		}

		public InputTypes InputType { get; set; }

		public object Data { get; set; }

		private object ParseData(InputTypes inputType, string data)
		{
			switch (inputType)
			{
				case InputTypes.Keyboard: return Functions.EnumParse<Key>(data);
				case InputTypes.Mouse: return Functions.EnumParse<MouseButtons>(data);
			}

			return null;
		}
	}
}
