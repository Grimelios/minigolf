﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Minigolf.Input.Data;
using Minigolf.Interfaces;
using OpenTK;
using OpenTK.Input;

namespace Minigolf.Input
{
	public class InputGenerator : IDynamic
	{
		private Game game;
		private MouseState oldMS;
		private MouseState newMS;
		private Point previousPosition;

		// It makes more sense to update key states each frame than to recreate the entire array every frame.
		private Dictionary<Key, InputStates> keyStates;

		private List<Key> newKeysDown;
		private List<Key> oldKeysDown;
		private List<Key> keysPressedLastFrame;
		private List<Key> keysReleasedLastFrame;

		public InputGenerator(Game game)
		{
			this.game = game;

			newKeysDown = new List<Key>();
			oldKeysDown = new List<Key>();
			keyStates = new Dictionary<Key, InputStates>();
			
			// These keys are needed for textboxes.
			Key[] mappedKeys =
			{
				Key.W,
				Key.S,
				Key.A,
				Key.D,
				Key.H,
				Key.Up,
				Key.Down,
				Key.Left,
				Key.Right,
				Key.Enter,
				Key.Back,
				Key.Escape,
				Key.Space,
				Key.ShiftLeft,
				Key.ShiftRight,
				Key.ControlLeft,
				Key.ControlRight,
				Key.Home,
				Key.End,
				Key.Insert,
				Key.Delete,
				Key.Keypad0,
				Key.Keypad1,
				Key.Keypad4,
				Key.Keypad6,
				Key.Keypad7,
				Key.KeypadDecimal,
				Key.KeypadEnter
			};

			foreach (Key key in mappedKeys)
			{
				keyStates.Add(key, InputStates.Released);
			}
		}

		public bool Alt { get; set; }
		public bool Shift { get; set; }
		public bool Control { get; set; }

		public void OnKeyPress(Key key)
		{
			newKeysDown.Add(key);
		}

		public void OnKeyRelease(Key key)
		{
			newKeysDown.Remove(key);
		}

		public void Update(float dt)
		{
			KeyboardData keyboardData = GenerateKeyboard();
			MouseData mouseDate = GenerateMouse();

			AggregateData aggregateData = new AggregateData
			{
				[InputTypes.Keyboard] = keyboardData,
				[InputTypes.Mouse] = mouseDate
			};

			Messaging.Send(MessageTypes.Keyboard, keyboardData, dt);
			Messaging.Send(MessageTypes.Mouse, mouseDate, dt);
			Messaging.Send(MessageTypes.Input, aggregateData, dt);
		}

		private KeyboardData GenerateKeyboard()
		{
			List<Key> keysPressedThisFrame = newKeysDown.Except(oldKeysDown).ToList();
			List<Key> keysReleasedThisFrame = oldKeysDown.Except(newKeysDown).ToList();

			keysPressedLastFrame?.ForEach(k => keyStates[k] = InputStates.Held);
			keysReleasedLastFrame?.ForEach(k => keyStates[k] = InputStates.Released);
			keysPressedThisFrame.ForEach(k => keyStates[k] = InputStates.PressedThisFrame);
			keysReleasedThisFrame.ForEach(k => keyStates[k] = InputStates.ReleasedThisFrame);
			keysPressedLastFrame = keysPressedThisFrame;
			keysReleasedLastFrame = keysReleasedThisFrame;

			oldKeysDown.Clear();
			oldKeysDown.AddRange(newKeysDown);

			return new KeyboardData(keyStates, newKeysDown, keysPressedThisFrame, keysReleasedThisFrame)
			{
				Alt = Alt,
				Shift = Shift,
				Control = Control
			};
		}

		private MouseData GenerateMouse()
		{
			oldMS = newMS;
			newMS = Mouse.GetCursorState();

			Point position = game.PointToClient(new Point(newMS.X, newMS.Y));

			InputStates leftClick = GetInputState(oldMS.LeftButton, newMS.LeftButton);
			InputStates rightClick = GetInputState(oldMS.RightButton, newMS.RightButton);
			InputStates middleClick = GetInputState(oldMS.MiddleButton, newMS.MiddleButton);
			InputStates side1Click = GetInputState(oldMS.XButton1, newMS.XButton1);
			InputStates side2Click = GetInputState(oldMS.XButton2, newMS.XButton2);

			float scrollDelta = newMS.WheelPrecise - oldMS.WheelPrecise;

			MouseData data = new MouseData(leftClick, rightClick, middleClick, side1Click, side2Click, position, previousPosition,
				scrollDelta);

			previousPosition = position;

			return data;
		}

		private InputStates GetInputState(ButtonState oldState, ButtonState newState)
		{
			if (oldState == newState)
			{
				return newState == ButtonState.Pressed ? InputStates.Held : InputStates.Released;
			}

			return newState == ButtonState.Pressed ? InputStates.PressedThisFrame : InputStates.ReleasedThisFrame;
		}
	}
}
