﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Minigolf.Input.Data;
using Minigolf.Interfaces;
using OpenTK;

namespace Minigolf.Input.Sets
{
	public class SelectableSet<T> where T : class, ISelectable
	{
		public SelectableSet() : this(new List<T>())
		{
		}

		public SelectableSet(T[] items)
		{
			Items = new List<T>(items);
		}

		public SelectableSet(List<T> items)
		{
			Items = items;
		}

		public List<T> Items { get; }
		public T HoveredItem { get; set; }
		public T FocusedItem { get; set; }

		public void HandleMouse(MouseData data)
		{
			Point mousePosition = data.Position;

			if (HoveredItem != null && !HoveredItem.Bounds.Contains(mousePosition))
			{
				HoveredItem.OnUnhover();
				HoveredItem = null;
			}

			foreach (T item in Items)
			{
				if (item.Enabled && item != HoveredItem && item.Bounds.Contains(mousePosition))
				{
					HoveredItem?.OnUnhover();
					HoveredItem = item;
					HoveredItem.OnHover();

					break;
				}
			}

			// Left click can't be rebound in this context.
			if (data.LeftClick == InputStates.PressedThisFrame)
			{
				if (HoveredItem != null && HoveredItem != FocusedItem && HoveredItem.OnSelect())
				{
					FocusedItem = HoveredItem;
				}
				else if (FocusedItem != null)
				{
					FocusedItem = null;
				}
			}
		}
	}
}
