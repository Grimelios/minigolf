﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Minigolf
{
	public enum EaseTypes
	{
		Linear,
		QuadraticIn,
		QuadraticOut,
		CubicIn,
		CubicOut
	}

	public static class Easing
	{
		public static float Ease(float value, EaseTypes easeType)
		{
			switch (easeType)
			{
				case EaseTypes.QuadraticIn: return QuadraticIn(value);
				case EaseTypes.QuadraticOut: return QuadraticOut(value);
				case EaseTypes.CubicIn: return CubicIn(value);
				case EaseTypes.CubicOut: return CubicOut(value);
			}

			// This is equivalent to EaseTypes.Linear.
			return value;
		}

		private static float QuadraticIn(float value)
		{
			return value * value;
		}

		private static float QuadraticOut(float value)
		{
			return -(value * (value - 2));
		}

		private static float CubicIn(float value)
		{
			return value * value * value;
		}

		private static float CubicOut(float value)
		{
			float f = value - 1;

			return f * f * f + 1;
		}
	}
}
