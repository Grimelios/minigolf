﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Minigolf.Entities;
using Minigolf.Interfaces;
using Minigolf.Settings;
using OpenTK;

namespace Minigolf
{
	public enum CameraModes
	{
		Fly,
		Follow,
		Static
	}

	public class Camera3D : IPositionable3D, IOrientable, IDynamic
	{
		private float speed;
		private float followDistance;

		private Matrix4 projection;

		public Camera3D()
		{
			Orientation = Matrix3.Identity;
			FollowDistance = CameraConstants.StartingDistance;

			Messaging.Subscribe(MessageTypes.VideoSettings, (data, dt) =>
			{
				VideoSettings settings = (VideoSettings)data;

				projection = Matrix4.CreatePerspectiveFieldOfView(settings.FieldOfView, Resolution.WindowAspect, CameraConstants.NearPlane,
					CameraConstants.FarPlane);
			});
		}

		public Vector3 Position { get; set; }
		public Vector3 Direction { get; set; }

		public Matrix3 Orientation { get; set; }
		public Matrix4 View { get; private set; }

		// It sort of makes sense to store the projection matrix inside the camera (since projection can be thought of as the camera lens).
		// The matrix is also useful for billboarding textures in the world.
		public Matrix4 ViewProjection { get; private set; }

		public float Yaw { get; set; }
		public float Pitch { get; set; }
		public float Roll { get; set; }

		public float Speed
		{
			get => speed;
			set => speed = MathHelper.Clamp(value, 0, CameraConstants.MaxSpeed);
		}

		public float FollowDistance
		{
			get => followDistance;
			set => followDistance = MathHelper.Clamp(value, CameraConstants.MinDistance, CameraConstants.MaxDistance);
		}

		public bool Decelerating { get; set; }

		public CameraModes Mode { get; set; }

		public Entity Target { get; set; }

		public void Update(float dt)
		{
			switch (Mode)
			{
				case CameraModes.Fly: UpdateFly(dt);
					break;

				case CameraModes.Follow: UpdateFollow();
					break;
			}

			ViewProjection = View * projection;
		}

		private void UpdateFly(float dt)
		{
			if (Decelerating)
			{
				speed -= CameraConstants.Deceleration * dt;

				if (speed < 0)
				{
					speed = 0;
				}
			}

			Position += Direction * speed * dt;
			Orientation =
				Matrix3.CreateRotationX(Pitch) *
				Matrix3.CreateRotationY(Yaw) *
				Matrix3.CreateRotationZ(Roll);

			View =
				Matrix4.CreateTranslation(-Position) *
				Orientation.ToMatrix4();
		}

		private void UpdateFollow()
		{
			Orientation =
				Matrix3.CreateRotationX(Pitch) *
				Matrix3.CreateRotationY(Yaw) *
				Matrix3.CreateRotationZ(Roll);

			Vector3 direction = Vector3.Transform(-Vector3.UnitZ, Orientation);
			Vector3 lookAt = Target.Position;

			Position = lookAt - direction * followDistance;
			View = Matrix4.LookAt(Position, lookAt, Vector3.UnitY);
		}
	}
}
